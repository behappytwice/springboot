package com.behappytwice.example.spring.websocket.stomp;

import org.slf4j.Logger;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;

import com.behappytwice.framework.log.Log;


/** 
 * Stomp를 사용한 웹소켓을 테스트하기 위한 컨르롤러이다. 
 * {@literal @}RequestMapping은 사용하지 않는다.
 * {@literal @}MessageMapping을 사용하여 클라이언트가 보낸 메시지를 수신하고
 * {@literal @}SendTo를 사용하여 구독하고 있는 클라이언트에게 메시지를 전송한다.
 *  
 * @author behap
 *
 */
@Controller
@RequestMapping(value="/sample/spring/websocket/stomp")
public class ExStompController {
	
	
	/** Logger */
	protected static @Log Logger logger;

	
	/** 
	 * WebSocketConfig에서 설정한 destinationprefix + 클라이언트가 전송할 mapping 주소
	 * 클라이언트가 /app/in을 붙여 서버로 메시지를 전달한다.
	 */
	@MessageMapping("/in/tt")
	/**
	 * /topic/in을 구독하고 있는 클라이언트에게 데이터를 전송한다
	 */
	@SendTo("/topic/in")
	/**
	 * 
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public ExStompMessage greeting(ExStompMessage message) throws Exception {
		//Thread.sleep(1000); // simulated delay
		//return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
		
		return message;
	}//:
	
	
	/** WebSocket 테스트 화면 */
	@RequestMapping(value="/stomp")
	public String stomp() {
		return "sample/spring/websocket/stomp/SampleWebSocketStomp";
	}//
	
}//~
