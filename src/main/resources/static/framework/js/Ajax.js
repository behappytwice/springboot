export default class Ajax {

	// see : https://github.com/axios/axios
	constructor() {

		this.requestConfig  = {
		   method:'get', // default  
		   // baseURL : '', // url이 절대경로가 아니면 baseURL은 url이 접두어로 붙여진다.
		   data : {} , // 전송 데이터
		   method  : "get",
		   timeout : 10000, // request time out
		   // see :  https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/responseType
		   // XMLHTTPRequest.responseType
		   //
		   // ""          : text와 같다. 
		   // arraybuffer : binary data를 포함하는 ArrayBuffer   
		   // blob        : binary data를 포함하는 Blob 
		   // document    : XML, HTML
		   // json        : JSON 
		   // text        : arraybuffer와 비슷한다. 그러나 데이터가 스트림으로 수신된다. 
		   //               이 응답 유형을 사용할 때 응답의 값은 progress 이벤트에 대한 핸들러에서만 사용할 수 있으며 
		   //               요청이 전송 된 이후에 수신 된 누적 데이터를 포함한다.         
		   responseType: 'json', // 서버가 응답할 데이터 타입 [json, text, stream, document, arraybuffer] 
		}
	}// constructor 
	
	ajax(config) {
		//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
		let returnedTarget = Object.assign(this.requestConfig, config); // only in ES6, use jQuery in ES5
		
		if(returnedTarget.responseType != "json") {
			// 서버가 응답할 데이터 타입이 json이 아니면 
			//returnedTarget.headers = {};
		}
		// for debugging below 
		console.log("The requested URL:" + returnedTarget.url);
		
		axios(returnedTarget).then(function(response) {
			
			console.log("The response staus:" + response.status);
			console.log("The response's header:" + response);
			
			// see : https://github.com/axios/axios
			// Response Schema 
			// {
			//   data : {}
			//   status : 200,
			//   statusText: 'OK',
			//   header: {}
			//   config : {}
			//   request : {}
			// }
			//
			// ResponseMessage Schema 
			//   status : 응답 코드
			//   statusText: 응답메시지
			//   description : 상세 응답메시지
			//   message : 응답 데이터
			// 
			// 응답 데이터의 status 필드가 있는지 체크한다. 응답 데이타가 존재하면 서버에서 
			// ResponseMessage 객체를 JSON으로 결과를 반환한 것이다.
			// 
			console.log("response status:" + response.status);
			
			if(response.data.status) {
				// 응답 메시지 status가 200이면 성공
				if(response.data.status == '200') {
					// vue component에 대한 참조가 유효하면 
					if(returnedTarget.vue) {
						returnedTarget.vue.$bvModal.show("modal-callback");
					}
					config.success(response);
				} else { // 응답 status가 200이 아니면 오류 발생
					// 세션이 없거나, 실패한 경우
					if(response.data.status ==  'no-session') {
						alert("세션 없음");
					}else {
						config.fail(response);	
					}
				}
			}else { 
				// 서버에서  ResponseMessage 객체를 사용하지 않고 결과를 반환함 
				config.success(response);
			}
		})
		.catch(function(error) {
			if(error.response) {
				// 요청을 보냈고 서버가 200 이 아닌 status code로 응답한 경우
				console.log(error.response.data);
				console.log(error.response.status);
				console.log(error.response.headers);
				config.fail(error)
			}else if(error.requst) {
				// 요청을 보냈고 서버로 부터 응답을 못 받은 경우
				console.log(error.request);
				config.fail(error)
			}else  {
				// 요청을 보내는 과정에서 에러가 발생한 경우
				console.log('Error', error.message);
				config.fail(error)
			}
			 //console.log(error.config);
		})
	}//ajax 
}///~