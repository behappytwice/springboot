package com.behappytwice.core.domain.model;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.behappytwice.app.biz.base.constants.enums.DataStatusEnum;
import com.behappytwice.framework.constants.FrameworkConstants;

public class ModelTestCase {

	@Test
	public void enumTest() {
		
		DataStatusEnum status = DataStatusEnum.CREATED;
		
		System.out.println(status.getStatus());
		System.out.println(status.DELETED.getStatus());
	}
	
	@Test
	public void enumTest2() {
		
		System.out.println(HttpStatus.INTERNAL_SERVER_ERROR.value());
		System.out.println(HttpStatus.INTERNAL_SERVER_ERROR);
		System.out.println(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		
	}
	

}
