import Ajax     from '/framework/js/Ajax.js';
import Utils    from '/framework/js/Utils.js';
import EventBus from '/framework/js/EventBus.js';

export default {
  template: `
     <div>
        
	    <b-card v-for="item in articles">
		    <div>
		       <div class="main-card-answer">
		           Answer Police Procedures Topic you might like		        
		       </div>
		       <div  class="main-card-title">
		          {{item.subject}}
		       </div>
		       <div>
		          <div class="main-card-author">
		            <img src="/app/portal/image/test.png" width="50" height="50">
		            {{item.creatorID}}
		          </div>
		          <div class="main-card-updated">{{item.lastModifiedTime}}</div>
		          <div class="main-card-content">
		           {{item.content}}
		           </div>
		           <div> 
		              bottom
		           </div>
		       </div>
		    </div>
  	    </b-card>
  	    
	  </div>
  `,
    // data section 
  data() {
    return {
    	articles:[]
    }
  },
  // constructor section 
  created() {
	  this.selectAllArticles();
  },
  mounted() {
  },
  computed : {
  },
  // method section 
  methods: {
	  /** 모든 게시글을 조회한다. */
	  selectAllArticles : function() {
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  vue:this, // vue 참조
				  url : '/board/article/selectAllArticles',
				  success : function(response) {
					  self.articles = response.data.message
				  },
				  fail: function(error) {
					  console.log("Internal Server Error");
				  }
		  };
		  new Ajax().ajax(requestConfig); 
	  }//:
  }// methods
} //export 
