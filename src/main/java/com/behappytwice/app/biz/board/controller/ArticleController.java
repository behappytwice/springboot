package com.behappytwice.app.biz.board.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.behappytwice.app.biz.base.service.EntryService;
import com.behappytwice.app.biz.board.entity.Article;
import com.behappytwice.app.biz.board.service.ArticleService;
import com.behappytwice.framework.base.component.BaseController;
import com.behappytwice.framework.util.web.HtmlFormUtils;
import com.behappytwice.framework.web.message.ResponseMessage;

@Controller
@RequestMapping(value="/board/article")
public class ArticleController extends BaseController {
	
	@Autowired
	private ArticleService articleService;
	@Autowired
	private EntryService entryService;
	
	@RequestMapping(value="/articleTestCase")
	public String articleTestCase(HttpServletRequest request) {
		String form = HtmlFormUtils.makeForm(Article.class);
		request.setAttribute("form",form);
		return "biz/board/test/articleTest";
	}
	
	
	
	@RequestMapping(value="/addArticle")
	public String insertArticle(HttpServletRequest request, HttpServletResponse response) {
		
		Article article = new Article();
		article.setContent(request.getParameter("content"));
		article.setSubject(request.getParameter("subject"));
		
		//article.setSubject("잘 가세요");
		//article.setContent("이것은 컨텐트입니다.");
		if(article == null) {
			logger.debug("■■■■■■■■■■■■■■■■■■■ article");
			return "/";
		}
		
		if(article.getContent() == null || article.getContent().equals("")) {
			logger.debug("■■■■■■■■■■■■■■■■■■■ getContent ");
			return "/";
		}
		if(article.getSubject() == null || article.getSubject().equals("")) {
			logger.debug("■■■■■■■■■■■■■■■■■■■ getSubjecte");
			return "/"; 
		}
		
		this.articleService.addArticle(article);
		String form = HtmlFormUtils.makeForm(Article.class);
		request.setAttribute("form",form);
		
		return "biz/board/test/articleTest";
	}//:
	
	@RequestMapping(value="/selectArticle")
	public String selectArticle() {
		String entryId = "0f0eb4a040724f5b81770689dc7cd0fa";
		Article article = this.articleService.selectArticle(entryId);
		return "/";
	}

	@RequestMapping(value="/modifyArticle")
	public String updateArticle() {
		String entryId = "0f0eb4a040724f5b81770689dc7cd0fa";
		//Entry entry = entryService.selectEntryById(entryId);
		Article article = new Article();
		article.setEntryId(entryId);
		article.setSubject("변경되었어");
		this.articleService.modifyArticle(article);
		return "/";
	}//:
	
	
	/**
	 * 모든 게시글을 조회한다. 
	 * @return
	 */
	@RequestMapping(value="/selectAllArticles")
	public @ResponseBody ResponseMessage selectAllArticles() {
		return this.createSuccessMessage(this.articleService.selectAllArticles());
	}//:
	/**
	 * 모든 게시글을 조회한다
	 * @return
	 */
	@RequestMapping(value="/selectAllArticles2")
	public @ResponseBody List<Article> selectAllArticles2() {
		return this.articleService.selectAllArticles();
	}//:

	
}///~
