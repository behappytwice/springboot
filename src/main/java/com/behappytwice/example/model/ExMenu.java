package com.behappytwice.example.model;

import lombok.Getter;
import lombok.Setter;

/** 테스트용 Menu 빈 */
@Getter
@Setter
public class ExMenu {
	private String manuId;
	private String name;
	private String url;

	
}
