package com.behappytwice.example.database.jpa.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.behappytwice.framework.base.BaseBean;
import com.behappytwice.framework.util.IDGenerator;
import com.behappytwice.example.database.jpa.entity.ExJpaEmployeeBean;

public class ExJpaEmployeeRepositoryImpl extends BaseBean implements ExJpaEmployeeRepositoryCustom {

	/** EntityManger instance */
	@PersistenceContext private EntityManager entityManaber;
	public void setEntityManager(EntityManager em) {
		this.entityManaber = em;
	}
	
	
	@Override
	public List<ExJpaEmployeeBean> myList() {
		
		ExJpaEmployeeBean employee = new ExJpaEmployeeBean();
		employee.setUserId(IDGenerator.generateID());
		employee.setName("kim park");
		List<ExJpaEmployeeBean> list  = new ArrayList<ExJpaEmployeeBean>();
		list.add(employee);
		return list;
	}
	
	
	public List<ExJpaEmployeeBean> selectEmployeeTest() {
		// 인스턴스 생성
		CriteriaBuilder builder = entityManaber.getCriteriaBuilder();
		// Query 오브젝트 생성
		CriteriaQuery<ExJpaEmployeeBean> query = builder.createQuery(ExJpaEmployeeBean.class);
		// FROM 구 변수 설정
		Root<ExJpaEmployeeBean> root = query.from(ExJpaEmployeeBean.class);
		//쿼리 결과 타입 명시
		query.select(root);
		// Query인스턴스를 생성하여 쿼리 준비
		Query q = entityManaber.createQuery(query);
		// 쿼리 실행 
		List<ExJpaEmployeeBean> employees=q.getResultList();
		return employees;
	}//:
	
	
	
	public List<ExJpaEmployeeBean> selectEmployeeTest2() {
		// 인스턴스 생성
		CriteriaBuilder builder = entityManaber.getCriteriaBuilder();
		// Query 오브젝트 생성
		CriteriaQuery<ExJpaEmployeeBean> query = builder.createQuery(ExJpaEmployeeBean.class);
		// FROM 구 변수 설정
		Root<ExJpaEmployeeBean> root = query.from(ExJpaEmployeeBean.class);
		//쿼리 결과 타입 명시
		query.multiselect(root.get("userId"), root.get("name"));
		// Query인스턴스를 생성하여 쿼리 준비
		Query q = entityManaber.createQuery(query);
		// 쿼리 실행 
		List<ExJpaEmployeeBean> employees=q.getResultList();
		return employees;
	}//:
	
	
	public ExJpaEmployeeBean selectSingleEmployee() {
		// 인스턴스 생성
		CriteriaBuilder builder = entityManaber.getCriteriaBuilder();
		// Query 오브젝트 생성
		CriteriaQuery<ExJpaEmployeeBean> query = builder.createQuery(ExJpaEmployeeBean.class);
		// FROM 구 변수 설정
		Root<ExJpaEmployeeBean> root = query.from(ExJpaEmployeeBean.class);
		//쿼리 결과 타입 명시
		query.select(root).where(  builder.equal(root.get("userId"), "2d9f703741ee406daa65356ee30b4ed9"));
		// Query인스턴스를 생성하여 쿼리 준비
		Query q = entityManaber.createQuery(query);
		// 쿼리 실행 
		ExJpaEmployeeBean employee=(ExJpaEmployeeBean)q.getSingleResult();
		return employee;
	}
	
	public List<ExJpaEmployeeBean> findEmployeesTest() {
		
		// https://yellowh.tistory.com/114
		// https://www.objectdb.com/java/jpa/query/criteria
		// https://www.boraji.com/hibernate-5-criteria-query-example
		CriteriaBuilder builder = entityManaber.getCriteriaBuilder();
		CriteriaQuery<ExJpaEmployeeBean> q = builder.createQuery(ExJpaEmployeeBean.class);
		Root<ExJpaEmployeeBean> root = q.from(ExJpaEmployeeBean.class);
		
		// where 절에 들어갈 목록
		List<Predicate> predicates = new ArrayList<>();
		// where 절에 like문추가
		predicates.add(builder.like(root.get("userName"), "%하하%"));
		
		return null;
	}
	
}///~
