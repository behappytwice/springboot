package com.behappytwice.example.java.enums;


/** 문자열만을 담은 enum이다. */
public enum SampleWhoEnum {

	KOREA("Korea"),
	AMERICA("SHA3-256"),
	UNKNOWN("");
	
	private String country;
	public String getCountry() {
		return this.country;
	}
	
	SampleWhoEnum(String country) {
		this.country  = country; 
	}
}
