package com.behappytwice.example.database.mybatis;

import org.apache.ibatis.type.Alias;

@Alias("TestSampleUSer")
public class ExMybatisBean {

	private String id;
	private String name;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
