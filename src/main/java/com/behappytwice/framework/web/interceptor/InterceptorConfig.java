package com.behappytwice.framework.web.interceptor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InterceptorConfig {

	/**
	 * 인터셉터의 이름
	 */
	private String name;
	/**
	 * 인터셉터의 설명
	 */
	private String description;
	/**
	 * 인터셉터가 처리할 URL Path
	 * 
	 */
	private String[] addPathPattern;
	/**
	 * Interceptor가 처리하지 않을 URL Path
	 */
	private String[] excludePathPattern;

	
	
}///~
