import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';
import TestVar from '/framework/js/TestVar.js';


export default {
  // view section 
  template: `
     <div class="content">
        {{user.name}}, {{user.userId}}
     </div>
  `,
  // constructor section 
  created() {
	  EventBus.$on('EVENT_GET_USER', this.getUser);
  },
  // data section 
  data() {
    return {
    	// event name
    	EVENT_GET_USER : 'EVENT_GET_USER',
    	user:{}
    }
  },
  // method section 
  methods: {
	  getUser: function(userId) {
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  url : '/vue/getUser',
				  data : { userId: userId},
				  success : function(response) {
					  self.user = response.data; // 저장해 놓은 self 사용
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Utils().ajax(requestConfig);
	  }
  }
} //export 
