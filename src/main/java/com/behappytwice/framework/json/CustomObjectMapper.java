package com.behappytwice.framework.json;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * ObjectMappepr를 상속하여 커스터마이징 한다. 
 * 
 * @author behap
 *
 */
public class CustomObjectMapper extends ObjectMapper {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new custom object mapper.
	 */
	public CustomObjectMapper() {
		
		SimpleModule simpleModule = new SimpleModule();
		simpleModule.addSerializer(java.util.Date.class, new DateSerializer());
		simpleModule.addDeserializer(java.util.Date.class, new DateDeserializer());
		registerModule(simpleModule);
	}
}///~
