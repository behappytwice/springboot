package com.behappytwice.framework.util.web;

import java.lang.reflect.Field;
import java.util.List;

import com.behappytwice.framework.util.ReflectionUtils;

/**
 * 
 * @author behap
 * @deprecated
 *
 */
public class HtmlFormUtils {
	
	public static String makeForm(Class clazz) {
		
		List<Field> fldList = ReflectionUtils.getAllFields(clazz);
		StringBuilder sb = new StringBuilder();
		
		sb.append("<h2><span id=\"title\">Insert/Update Test Form</span></h2>");
		
		String formHeader = "<form name=\"form1\" id=\"form1\"><br>\n";
		sb.append("<table>");
		sb.append(formHeader);
		fldList.forEach( item -> {
		   sb.append("<tr>");
		   String element = String.format("<td>%s </td><td> <input type=\"text\" name=\"%s\" id=\"%s\"></td>" ,item.getName(), item.getName(), item.getName());
		   sb.append(element);
		   sb.append("</tr>\n");
		});
		sb.append("</table>");
		sb.append("<button type=\"button\" name=\"button1\" id=\"button1\">Submit</button>");
		String formFooter = "</form>";
		sb.append(formFooter);
		
		return sb.toString();
	}
	
}///~
