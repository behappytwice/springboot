package com.behappytwice.framework.util.json;

import java.util.ArrayList;
import java.util.List;

import com.behappytwice.framework.json.CustomObjectMapper;
import com.behappytwice.framework.json.CustomSerializerProvider;
import com.behappytwice.framework.util.ClassPathFileUtils;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * JSON를 다루기 위한 편리한 메소드들을 제공한다.
 * 
 * @author behap
 *
 */
public class JSONUtils {

	
	/** CustomObjectMapper를 생성합니다. */
	private static CustomObjectMapper createObjectMapper() {
		CustomObjectMapper mapper = new CustomObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setSerializerProvider(new CustomSerializerProvider());
		return  mapper;
	}//:  
	
	/**
	 * Java Object를 JSON으로 변환한다. 
	 * 
	 * @param object Java Object 
	 * @return
	 * 		JSON 문자열 
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 */
	public static String toJSON(Object object) throws JsonGenerationException, JsonMappingException, JsonProcessingException  {
		CustomObjectMapper mapper = createObjectMapper();
		String jsonInString = mapper.writeValueAsString(object);
		return jsonInString;
	}//:
	
	/**
	 * JSON을 객체로 변환한다. 
	 * @param jsonString JSON 문장졀 
	 * @param clazz Class Type
	 * @return
	 * 		Type으로 전달된 객체를 반환한다.
	 */
	public static <T> Object toObject(String jsonString, Class<T> clazz) {
		try { 
			CustomObjectMapper mapper = createObjectMapper();
			return mapper.readValue(jsonString, clazz);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}//:
	
	
	/**
	 * Java 객체를 담은 List를 반환한다. 
	 * 
	 * @param path  파일 경로 
	 * @param t Class Type
	 * @return
	 * 		파라미터로 넘어 온 자바객체를 담은 리스트를 반환 
	 * @throws Exception
	 */
	public static <T> List<T> toList(String path, Class<T> t) throws Exception {
		try {
			CustomObjectMapper mapper = createObjectMapper();
			JavaType jType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, t);
//			TypeReference tr = new TypeReference<ObjectArrayForJSON<T>>() {};
			List<T> i18bean = mapper.readValue(ClassPathFileUtils.getFileObject(path), jType);
			return i18bean;
		} catch (Exception e) {
			// Ignores exception
			// e.printStackTrace();
			return new ArrayList<T>();
		}
	}// :
	
}///~
