package com.behappytwice.framework.base.component;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.behappytwice.app.biz.base.constants.enums.DataStatusEnum;
import com.behappytwice.app.biz.base.entity.TimeAndAccessorInfo;
import com.behappytwice.framework.base.BaseBean;
import com.behappytwice.framework.base.exception.BizException;


/**
 * 모든 Service 클래스의 슈퍼클래스이다. Service 클래스를 구현할 때 이 클래스를 상속 받아야 한다. 
 * @author behap
 *
 */
@Component
public class BaseService extends BaseBean {
	
	protected void assertNullThrowBizException(Object o, String code, String message ) throws BizException {
		if(o == null) {
			throw new BizException(code, message);
		}
	}
	
	
	protected void assertNullThrowException(Object o ) throws Exception {
		if(o == null) {
			throw new Exception("Object is null");
		}
	}
	

	/**
	 * 모든 Entity 클래스는 생성된 시간, 수정된 시간 속성을 가지고 있다.
	 * 엔티티를 저장하기 전에 생성된 시간, 수정된 시간, 데이터의 상태를 설정할 때 사용한다.
	 * @param bean 시간 및 상태정보를 설정할 데이터 
	 */
	protected void setTimeAndAccessorInfoForCreation(TimeAndAccessorInfo bean) { 
		Date createdTime = new Date(); 
		bean.setCreatedTime(createdTime);
		bean.setLastModifiedTime(createdTime);
		bean.setDataStatus(DataStatusEnum.CREATED.getStatus());		
	}//:
	
}///~
