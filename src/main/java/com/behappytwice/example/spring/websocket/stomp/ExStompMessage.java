package com.behappytwice.example.spring.websocket.stomp;

import lombok.Getter;
import lombok.Setter;

/**
 * Stomp 채팅 테스트 메시지 
 * @author behap
 *
 */
@Getter
@Setter
public class ExStompMessage {
	/** 사용자 아이디 */
	private String userId;
	/** 사용자 명*/
	private String userName;
	/** 전달 메시지 */
	private String message;

}///~
