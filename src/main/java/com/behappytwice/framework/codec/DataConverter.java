package com.behappytwice.framework.codec;

/** 데이터의 타입을 변환하는 기능을 지원한다. */
public class DataConverter {
	
	static final HexConverter HEX_CONVERTER  = new HexConverter();
	/**
	 * HexConverter 인스턴스를 반환한다. 
	 * @return
	 */
	public static HexConverter getHexConverter() {
		return HEX_CONVERTER;
	}

	/**
	 * Hex 문자열을 byte 배열로 byte 배열을 hex 문자열로 변환합니다.  
	 * @author behap
	 *
	 */
	public static class HexConverter {
		public HexConverter() {}
		
		/**
		 * 바이트 배열을 16진수값으로 변환한다.
		 * 
		 * @param hash hex값으로 변환할 byte 배열
		 * @return
		 * 		hex로 변환된 문자열 
		 */
		public String bytesToHex(byte[] hash) {
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		}//:
		
		/**
		 * Hex 값으로 이루어진 문자열을 바이트 배열로 변환한다. 
		 * @param s 바이트로 변환할 문자열 
		 * @return
		 */
		public byte[] hexToBytes(String s) {
		    int len = s.length();
		    byte[] data = new byte[len / 2];
		    for (int i = 0; i < len; i += 2) {
		        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
		                             + Character.digit(s.charAt(i+1), 16));
		    }
		    return data;
		}//:
		
		/**
		 * 문자열을 Hex 문자열로 변환한다. 
		 * @param s 변환할 문자열 
		 * @return
		 * 		Hex 값으로 변환된 문자열 
		 */
		public String strToHex(String s) {
			return bytesToHex(s.getBytes());
		}
		/**
		 * Hex 문자열을 원래의 문자열로 변환한다. 
		 * @param s Hex 문자열 
		 * @return
		 * 		변환된 문자열 
		 */
		public String hexToString(String s) {
			return new String(hexToBytes(s));
		}
	}///~
	
	

}///~
