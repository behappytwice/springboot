package com.behappytwice.example.spring.websocket;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;


/** WebSocketHandler 이다. */
@Component
public class ExWebSocketHandler extends TextWebSocketHandler {

	/** 서버에 연결한 사용자들을 저장하는 리스트 */
	private List<WebSocketSession> connectedUsers;
	
	/**
	 * WebSocketSession을 저장할 List를 생성한다.
	 */
	public ExWebSocketHandler() {
		// Create a List
		this.connectedUsers = new ArrayList<WebSocketSession>();
	}

	/**
	 * 웹소켓이 연결되었을 때
	 */
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		connectedUsers.add(session);
		System.out.println("사용자가 방금 접속했습니다.");
	}
	/** 메시지를 수신 받았을 때 */
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		for (WebSocketSession webSocketSession : connectedUsers) {
			// 자신이 보낸 메시지는 다시 보내지 않음
			if(!session.getId().equals(webSocketSession.getId())) { 
				webSocketSession.sendMessage(new TextMessage( message.getPayload()));
			}
		}
		System.out.println("다음의 메시지가 도착했습니다." + message.getPayload());
	}// :
	/**
	 * 사용자의 접속이 끊어 졌을 때 
	 */
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		connectedUsers.remove(session);
		System.out.println("사용자와 접속이 끊어 졌습니다.");
	}
}/// ~
