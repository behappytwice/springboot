package com.behappytwice.framework.util.json;

import java.util.List;

import org.junit.Test;

import com.behappytwice.example.model.ExUserBean;

/** JSONUtils 테스트케이스 */
public class JSONUtilsTest {
	
	
	/** POJO를 JSON으로 변환하는 테스트 */
	@Test
	public void testPojoToJson() throws Exception{
		ExUserBean user = new ExUserBean();
		user.setUserId("1111");
		user.setUserName("홍길동");;
		String json = JSONUtils.toJSON(user);
		System.out.println(json);
		//{"userId":"1111","userName":"홍길동","enteredDate":null,"email":"","address":"","jobs":[]}
	}//:
	
	
	/** JSON을 POJO로 변경하는 테스트 */
	@Test
	public void testJsonToPojo() {
		//{"userId":"1111","userName":"홍길동","enteredDate":null,"email":"","address":"","jobs":[]}
		String json = "{\"userId\":\"1111\",\"userName\":\"홍길동\",\"enteredDate\":null,\"email\":\"\",\"address\":\"\",\"jobs\":[]}";
		ExUserBean user = (ExUserBean)JSONUtils.toObject(json, ExUserBean.class);
		System.out.println(user.getUserName());
		System.out.println(user.getUserId());
	}//:
	
	/** Classpath에 있는 JSON 파일을 읽어서 리스트로 변환하는 테스트 */
	@Test
	public void testJsonToList() throws Exception {
		// i18n.json 파일에 들어 있는 내용
//		[
//		    "com/behappytwice/app/i18n/messages"
//		]
		List<String> list = JSONUtils.toList("i18n.json", String.class);
		for(String item : list) {
			System.out.println(item);
		}
	}//:
	
	
}//~
