import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';
import SignUpForm from './SignUpForm.js';
import SignInForm from './SignInForm.js';

new Vue( {
  el: '#app',
  template: `
     <div>
         <b-container>
            <b-row>
              <b-col>
                 <div>
                     <h2>Faster release</h2>
                     <div>
                     Hello! Thanks for your visiting. I hope this site is helpful to you, guys.It all works, 
                     but there is some repetition and the route guard is a bit nasty. If you want to change the lang param in the html tag or set the axios Accept-language header, 
                     those would need to fit into the beforeEnter guard too.
What about checking for supported languages? Those are defined in a few places. Not really DRY.
What you could do is extract all those repetitions and fit them inside a helper service 
file that you can then import and use. You could also extract this into Vuex actions, 
but because it will not be used inside Vue components that often, it doesn’t make sense. 
That would also make them async.
                     </div>
                 </div>
                 
              </b-col>
              <b-col>
                    <b-tabs content-class="mt-3">
	  					<b-tab title="Sign In" active>              
	  					      <sign-in-form></sign-in-form>
	  					</b-tab>
        				<b-tab title="Sign Up">
        				      <sign-up-form></sign-up-form>
        				</b-tab>
	  				</b-tabs>
              </b-col>
              <b-col>
              </b-col>
            </b-row>
         </b-container>
         
     </div>
  `,
  
  components : {
	SignUpForm,
	SignInForm
  },
  // constructor section 
  created() {
  },
  // data section 
  data() {
    return {
    }
  },
  // method section 
  methods: {
  }
}); //export 
