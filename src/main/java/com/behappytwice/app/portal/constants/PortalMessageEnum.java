package com.behappytwice.app.portal.constants;

public enum PortalMessageEnum {

	/**
	 * reason pharase는 프러퍼티 파일의 키를 저장할 것을 고려해야 함 
	 */
	USER_MISMATCH_EXCEPTION("U100", "사용자 정보를 다시 확인하여 주세요");
	
	private String value;
	private String reasonPhrase;
	
	public String getValue() {
		return this.value; 
	}
	
	public String getReasonPhrase() { 
		return this.reasonPhrase;
	}
	
	PortalMessageEnum(String value, String reasonPhrase) {
		this.value = value;
		this.reasonPhrase = reasonPhrase;
	}//:

}///~
