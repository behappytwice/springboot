import Ajax from '/framework/js/Ajax.js';
import EventBus from '/common/js/EventBus.js';

export default {
  template: `
     <div>
		  <b-nav>
		    <b-nav-item active>Active</b-nav-item>
		    <b-nav-item>Link</b-nav-item>
		    <b-nav-item>Another Link</b-nav-item>
		    <b-nav-item disabled>Disabled</b-nav-item>
		  </b-nav>
     </div>
  `,
  // constructor section 
  created() {
	  
  },
  // data section 
  data() {
    return {
    }
  },
  // method section 
  methods: {
  }
} //export 
