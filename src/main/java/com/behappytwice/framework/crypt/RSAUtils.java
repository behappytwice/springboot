package com.behappytwice.framework.crypt;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

/** RSA 암복화를 지원한다. */
public class RSAUtils {

	/**
	 * 공개키/개인키 KeyPair을 생성합니다.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static KeyPair createKeyPair() throws Exception {
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
		keyPairGen.initialize(2048);
		return keyPairGen.genKeyPair();
	}// :
	
	
	/**
	 * 공개키의 바이트 배열로부터 공개키를 생성한다.
	 * 
	 * @param keyBytes 공개키의 바이트 배열
	 * @return
	 * @throws Exception
	 */
	public static PublicKey createPublicKey(byte[] keyBytes) throws Exception {
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyFactory.generatePublic(keySpec);
		return publicKey;
	}// 
	

	/**
	 * 공개키/개인키를 파일로 부터 읽어들인다.
	 * 
	 * @param filePath
	 *            저장된 파일명(풀패스)
	 * @return 키의 바이트배열
	 * @throws Exception
	 *             the Exception
	 */
	public static byte[] getKeyFromFile(String filePath) throws Exception {

		FileInputStream fis = new FileInputStream(filePath);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int theByte = 0;
		while ((theByte = fis.read()) != -1) {
			baos.write(theByte);
		}
		fis.close();
		byte[] keyBytes = baos.toByteArray();
		baos.close();

		return keyBytes;
	}// :
	
	/**
	 * 공개키로 바이트를 암호화 한다. 
	 * 
	 * @param bytesToEncrypt 암호화할 바이트 배열
	 * @param publicKey 공개키
	 * @return
	 * 		암호화된 바이트 배열 
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] bytesToEncrypt, PublicKey publicKey) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		// Perform the actual encryption on those bytes
		byte[] cipherText = cipher.doFinal(bytesToEncrypt);
		return cipherText;
	}// :
	
	/**
	 * 개인키로 암호화된 바이트 배열을 복호화 한다. 
	 * @param bytesToDecrypt 복호화 할 바이트 배열
	 * @param privateKey 개인키 
	 * @return
	 * 		복호화된 바이트 배열 
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] bytesToDecrypt, PrivateKey privateKey) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return cipher.doFinal(bytesToDecrypt);
	}// :

}///~
