package com.behappytwice.app.biz.base.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "role_player")
@Inheritance(strategy=InheritanceType.JOINED)
//@DiscriminatorColumn(name="rolePlayerType")
//@DiscriminatorColumn(name="role_player_type")
@Getter
@Setter
public class RolePlayer extends TimeAndAccessorInfo {

	@Id
	@Column(name="role_palyer_id")
	protected String rolePlayerID;
	@Column(name="name")
	protected String name;
	@Column(name="role_player_type")
	protected String rolePlayerType;
	
	
}
