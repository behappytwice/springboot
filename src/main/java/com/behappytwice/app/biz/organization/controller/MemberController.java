package com.behappytwice.app.biz.organization.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.behappytwice.app.biz.organization.entity.Member;
import com.behappytwice.app.biz.organization.service.MemberService;
import com.behappytwice.framework.base.component.BaseController;
import com.behappytwice.framework.web.message.ResponseMessage;

@Controller
@RequestMapping(value="/member")
public class MemberController extends BaseController {

	@Autowired
	private MemberService memberService;
	
	
	
	@RequestMapping(value="/insertMember")
	public @ResponseBody ResponseMessage insertMember(@RequestBody Member member) {
		this.memberService.insertMember(member);
		return this.createSuccessMessage(member);
	}
	
	
	
	@RequestMapping(value="/findByEmail")
	public @ResponseBody ResponseMessage findByEmail(@RequestBody Member member) {
		this.logger.debug(">>>>>>>>>>>>>>>>>> findByEmail");
		//String email = request.getParameter("email");
		this.logger.debug(">>> email:" + member.getEmail());
		Member member2 = this.memberService.findByEmail(member.getEmail());
		return this.createSuccessMessage(member2);
	}//;
}
