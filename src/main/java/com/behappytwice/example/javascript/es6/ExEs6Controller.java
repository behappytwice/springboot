package com.behappytwice.example.javascript.es6;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.behappytwice.framework.base.component.BaseController;

/**
 * ESMAScript 6 버전을 테스트하기 위한 컨트롤러이다.
 * @author behap
 *
 */
@Controller
@RequestMapping(value="/example/es6")
public class ExEs6Controller extends BaseController {

	/** 메인화면 */
	@RequestMapping(value="/main")
	public String main()  {
		return "sample/es6/Es6Main";
	}//:
}
