package com.behappytwice.app.portal.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.behappytwice.app.biz.organization.entity.Member;
import com.behappytwice.app.biz.organization.service.MemberService;
import com.behappytwice.app.portal.constants.PortalMessageEnum;
import com.behappytwice.framework.base.exception.BizException;
import com.behappytwice.framework.constants.FrameworkConstants;
import com.behappytwice.framework.util.web.CookieUtils;
import com.behappytwice.framework.web.session.SessionThreadLocal;
import com.behappytwice.framework.web.session.SessionUtils;
import com.behappytwice.framework.web.token.JavaTokenUtils;

@Service
public class PortalService {
	
	@Autowired
	private MemberService memberService;

	public void test(String id) {
		try { 
			Thread.sleep(10000);	
		}catch(Exception e) {
		}
		Map map = SessionThreadLocal.getValue();
		HttpServletRequest request = (HttpServletRequest)map.get("request");
		System.out.println(id + "====" + request.getAttribute("key"));
	}// :

	
	public void insertMember(Member member) {
		this.memberService.insertMember(member);
	}//:
	
	
	/**
	 * Find a member by the email address
	 * 
	 * @param email a user's email address
	 * @return
	 *    a Member instance
	 */
	public Member findByEmail(String email) { 
		return this.memberService.findByEmail(email);
	}
	
	/**
	 * Find a member by the name
	 * @param name  a user's name
	 * @return
	 * 	a member instance
	 *
	 */
	public Member findByName(String name) {
		return this.memberService.findByName(name);
	}
	
	
	public Member signIn(Member member) {
		Member preMember = this.memberService.findByEmail(member.getEmail());
		// The member not exists
		if (preMember == null) {
			PortalMessageEnum pm = PortalMessageEnum.USER_MISMATCH_EXCEPTION;
			throw new BizException(pm.getValue(), pm.getReasonPhrase());
		}
		if (preMember.getPassword() == null) {
			PortalMessageEnum pm = PortalMessageEnum.USER_MISMATCH_EXCEPTION;
			throw new BizException(pm.getValue(), pm.getReasonPhrase());
		}
		// The password not matched
		if (!preMember.getPassword().equals(member.getPassword())) {
			PortalMessageEnum pm = PortalMessageEnum.USER_MISMATCH_EXCEPTION;
			throw new BizException(pm.getValue(), pm.getReasonPhrase());
		}
		SessionUtils.setSignedUser(member);
		
		long expiration = 60 * 60 * 24 * 1000;
		Map<String, String> claims = new HashMap();
		claims.put(FrameworkConstants.TOKEN_USER_INFO_KEY, "hello");

		String jwsString = JavaTokenUtils.createJws("behappytwice", "all", expiration, claims);
		CookieUtils.addCookie( SessionThreadLocal.getServletResponse() ,FrameworkConstants.TOKEN_COOKIE_NAME, jwsString);
		
		return preMember;
	}// :
	
}/// ~