
export default class Utils {
	/**
	 * Validates an email.
	 */
	static validateEmail(email) {
		// 검증에 사용할 정규식 변수 regExp에 저장
		var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
		if (email.match(regExp) != null) {
			return true;
		}
		else {
			return false;
		}
	}//:
	
	static isEmpty(str) {
		// undefined, null, empty string are all false
		 return (str)? false:true;
	}
	
	

}// /~
