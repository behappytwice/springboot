package com.behappytwice.example.java.generic;

/** Java Generic을 테스트하기 위한 클래스이다. */
public class ExGenericUtils {
	
	
	/** 하나의 타입을 파라미터로 가지는 Generic Method */
	public static <T> String getAppendedString(T t) {
		return (String)t + " appended String";
	}//:
	
	/** 두개의 타입을 파라미터로 가지는 Generic Method */
	public static <T,V> String getAddedString(T t, V v) {
		return (String)t + (String)v;
	}

}///~
