package com.behappytwice.app.biz.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.behappytwice.app.biz.board.entity.Article;

public interface ArticleRepository extends JpaRepository<Article, String> {
	
	Article findByEntryId(String entryId);
	
//	@Query(value="SELECT a FROM article a")
//	List<Article> findAllArticles();

}///~
