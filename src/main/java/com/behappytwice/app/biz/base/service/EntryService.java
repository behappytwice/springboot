package com.behappytwice.app.biz.base.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.behappytwice.app.biz.base.entity.Entry;
import com.behappytwice.app.biz.base.repository.EntryRepository;
import com.behappytwice.framework.base.component.BaseService;

@Service
public class EntryService extends BaseService {

	@Autowired
	private EntryRepository entryRepository; 
	
	public Entry selectEntryById(String entryId) {
		return entryRepository.findByEntryId(entryId);
	}
}
