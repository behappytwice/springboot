package com.behappytwice.example.database.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.behappytwice.framework.util.IDGenerator;


@Service
public class ExMybatisService {

	
	@Autowired
	private ExMybatisMapper mybatisTestMapper;

	
	@Transactional( value="transactionManager", propagation = Propagation.REQUIRES_NEW)
	public void insert() {
		ExMybatisBean bean = new ExMybatisBean();
		bean.setId(IDGenerator.generateID());
		bean.setName("kim");
		
		
		this.mybatisTestMapper.insert(bean);
		bean = new ExMybatisBean();
		bean.setId(IDGenerator.generateID());
		bean.setName("kim");
		
		this.mybatisTestMapper.insert(bean);
	}//:
}
