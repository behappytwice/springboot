import Ajax from '/framework/js/Ajax.js';
import EventBus from '/common/js/EventBus.js';

export default {
  template: `
     <div>
			<b-list-group>
				  <b-list-group-item href="#">Default list group item</b-list-group-item>
				  <b-list-group-item href="#" variant="primary">Primary list group item</b-list-group-item>
				  <b-list-group-item href="#" variant="secondary">Secondary list group item</b-list-group-item>
				  <b-list-group-item href="#" variant="success">Success list group item</b-list-group-item>
				  <b-list-group-item href="#" variant="danger">Danger list group item</b-list-group-item>
				  <b-list-group-item href="#" variant="warning">Warning list group item</b-list-group-item>
				  <b-list-group-item href="#" variant="info">Info list group item</b-list-group-item>
				  <b-list-group-item href="#" variant="light">Light list group item</b-list-group-item>
				  <b-list-group-item href="#" variant="dark">Dark list group item</b-list-group-item>
			</b-list-group>   
     </div>
  `,
  // constructor section 
  created() {
	  
  },
  // data section 
  data() {
    return {
    }
  },
  // method section 
  methods: {
  }
} //export 
