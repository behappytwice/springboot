package com.behappytwice.app.biz.base.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Folder {
	
	protected String folderID;
	protected String subject;
	protected String parentFolderID;
}
