package com.behappytwice.app.biz.base.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class TimeAndAccessorInfo {

	@Column(name="created_time")
	protected Date createdTime;;
	@Column(name="last_modified_time")
	protected Date lastModifiedTime;
	@Column(name="creator_id")
	protected String creatorID;
	@Column(name="modifier_id")
	protected String modifierID;
	@Column(name="data_status")
	protected String dataStatus;

}
