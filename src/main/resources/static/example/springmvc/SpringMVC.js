import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';

new Vue({
  el: '#app',
  template : `
	     <div>
				<span @click="postXml">XML 전송하기</span> <br>
				<span @click="postUrlencoded">FORM DATA 전송하기</span> <br>
				<span @click="postJSON">JSON 전송하기</span><br>
				<span @click="postText">Text 전송하기</span><br>
				<span @click="postReqParam">쿼리스트링으로 파라미터 전달하기</span><br>
				<span @click="jsonTest">JSON 응답받기</span><br>
				<span @click="xmlTest">XML 응답받기</span><br>
				<span @click="htmlTest">HTML 응답받기</span><br>
				<span @click="textTest">Text 응답받기</span><br>
				<span @click="fileDownload">파일다운로드 하기</span><br>
         </div>
  `,
  created() {
  },
  components: {
  },
  data() {
	  return {
		  member : {
		  }
	  }
  },
  methods : {
	  // XML 전송하기 
	  postXml : function() {
		  
		  var xml = "" 
			  +	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			  + "<note>"
              + "  <to>Tove</to> "
              + "  <body>Don't forget me this weekend!</body> "
              + " </note>";
		  
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/postXml',
				  data : xml,
				  headers: {
					    'Content-type': 'application/xml',
					    'Accept' : 'application/json'
					  },
				  responseType : 'json',
				  success : function(response) {
					  alert("OK");
				  },
				  fail: function(error) {
					  alert("Error");
				  }
		  };
		  new Ajax().ajax(requestConfig);
		  
	  },
	  
	  // form data 전송 
	  postUrlencoded : function() {
		  
		  var member = "userId=1234356&userName=HongGilDong";
		  
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/postUrlencoded',
				  data : member,
				  headers: {
  					    'Content-type': 'application/x-www-form-urlencoded',
					    'Accept' : 'application/json'
					  },
				  responseType : 'json',
				  success : function(response) {
					  alert("OK");
				  },
				  fail: function(error) {
					  alert("Error");
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  
	  // json 전송 
	  postJSON : function() {
		  console.log("Started....");
		  var dateString = JSON.stringify(new Date());
		  var json = JSON.parse(dateString);
		  console.log((typeof dateString));
		  alert(dateString);
		  var member =  { userId : "123456", userName : "Hong Gil Dong" , enteredDate: json};
		  console.log(member);
		  console.log("JSON Mapped");
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/postJSON',
				  data : member,
				  headers: {
					  'Content-type': 'application/json',
					  'Accept' : 'application/json'
				  },
				  responseType : 'json',
				  success : function(response) {
					  alert("OK");
				  },
				  fail: function(error) {
					  alert("Error");
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  
	  // text 전송 
	  postText : function() {
		  
		  var text = "This is a sample text.";
		  
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/postText',
				  data : text,
				  headers: {
					  'Content-type': 'text/plain',
					  'Accept' : 'application/json'
				  },
				  responseType : 'json',
				  success : function(response) {
					  alert("OK");
				  },
				  fail: function(error) {
					  alert("Error");
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  
	  // query string 전송 
	  postReqParam : function() {
		  
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/reqParam?name=Andy',
				  headers: {
  					    'Content-type': 'application/x-www-form-urlencoded',
					    'Accept' : 'application/json'
					  },
				  responseType : 'json',
				  success : function(response) {
					  alert("OK");
				  },
				  fail: function(error) {
					  alert("Error");
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  
	  
	  
	  // json 데이터 받기(ResponseMessage 사용하지 않음)  
	  jsonTest : function() {
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/jsonTest',
				  responseType : 'json',
				  success : function(response) {
					  var json = response.data;
					  alert(json.userName);
				  },
				  fail: function(error) {
					  alert("Error");
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  // xml 데이터 받기 (ResponseMessage 사용하지 않음) 
	  xmlTest : function() {
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/xmlTest',
				  responseType : 'document',
				  success : function(response) {
					  var xml = response.data;
					  console.log(xml);
					  alert("OK");
				  },
				  fail: function(error) {
					  alert("Error");
				  }
				  
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  // html 데이터 받기 (ResponseMessage 사용하지 않음) 
	  htmlTest : function() {
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/htmlTest',
				  responseType : 'document',
				  success : function(response) {
					  var html = response.data;
					  console.log(html);
					  alert("OK");
				  },
				  fail: function(error) {
					  alert("Error");
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  
	  // html 데이터 받기 (ResponseMessage 사용하지 않음) 
	  textTest : function() {
		  var self = this; // this 저장해 놓아야 함
		  var requestConfig = {
				  method : 'post',
				  url : '/example/springmvc/textTest',
				  responseType : 'text',
				  success : function(response) {
					  alert(response.data);
					  alert("OK");
				  },
				  fail: function(error) {
					  alert("Error");
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  
	  fileDownload : function() {
		  
		  var self = this; // this 저장해 놓아야 함 
		  // pdf download
		  var requestConfig = {
				  method : 'get',
				  url : '/attachments/imagedown.pdf',
				  headers: { 'content-type': 'application/pdf' },
				  responseType : 'arraybuffer',
				  success : function(response) {
					  var blob = new Blob([response.data], {
						  type : 'applicaiotn/pdf'
					  })
					  alert(blob.size);
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  }
  }
});
