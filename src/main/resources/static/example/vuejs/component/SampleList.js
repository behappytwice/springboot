import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';

export default {
  template: `
     <div class="content">
            <h3>Sample List</h3>
            <ul>  
                <li v-for="item in userList">
                 <a href="#" @click="onItemClicked(item.userId)"> {{item.name}}</a>
                </li>
            </ul>
     </div>
  `,
  // constructor section 
  created() {
	  this.getUserList();
	  EventBus.$on('EVENT_GET_USERLIST', this.getUserList);
	  
  },
  // data section 
  data() {
    return {
    	userList: []
    }
  },
  // method section 
  methods: {
	  onItemClicked: function(userId) {
		  EventBus.$emit("EVENT_LIST_ITEM_CLICKED", userId);
	  },
	  getUserList: function() {
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  url : '/vue/getUserList',
				  success : function(response) {
					  self.userList = response.data; // 저장해 놓은 self 사용
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Utils().ajax(requestConfig);
	  }
  }
} //export 
