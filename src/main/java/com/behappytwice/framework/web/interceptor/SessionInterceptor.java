package com.behappytwice.framework.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;

import com.behappytwice.framework.auth.AuthUtils;
import com.behappytwice.framework.constants.FrameworkConstants;
import com.behappytwice.framework.log.Log;
import com.behappytwice.framework.web.message.ResponseMessage;
import com.behappytwice.framework.web.session.SessionUtils;

/**
 * 로그인하지 않은 사용자는 포탈화면으로 리다이렉트 시킨다. 클라이언트에게 반환할 형식이 JSON이면 ResponseMessage 객체를
 * JSON으로 변환하여 반환해야 한다. status 필드의 값에는 no-session를 설정한다.
 * 
 * @author behap
 *
 */
public class SessionInterceptor implements HandlerInterceptor {

	protected static @Log Logger logger;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
	
		// Token이 유효한지 체크 
		boolean isTokenValid =  AuthUtils.checkToken(request, response);
		
		if (SessionUtils.isSigned()) {
			return true;
		}
		if (isOnlyAcceptAvailable(request)) {
			// logger.debug("json 응답");
			ResponseMessage resMessage = new ResponseMessage();

			resMessage.setStatus(FrameworkConstants.NO_SESSION_CODE); // set an error code to the status
			resMessage.setStatusText(FrameworkConstants.NO_SESSION_CODE); // an error message
			response.setStatus(HttpStatus.OK.value());
			response.setCharacterEncoding("UTF-8"); // have to modify this
			response.setHeader("Content-Type", "application/json;charset=UTF-8");
			return false;
		}

		// static resource 요청도 inerceptor에 걸리기 때문에
		// interceptor.json에 session check할 URL을 모두 패턴을 정의해야 함

		if (!(request.getRequestURL().indexOf(FrameworkConstants.NO_SESSION_REDIRECT_URL) >= 0)) {
			response.sendRedirect(request.getServletContext() + FrameworkConstants.NO_SESSION_REDIRECT_URL);
		}
		return false;
	}// :

	/**
	 * Accept 헤더의 값이 application/json 하나만 존재하는지 확인한다.
	 * 
	 * @param request HttpServletRequest
	 * @return 하나만 존재하면 true, 아니면 false
	 */
	private boolean isOnlyAcceptAvailable(HttpServletRequest request) {
		String accept = request.getHeader("Accept");
		if (accept == null) {
			return false;
		}
		String[] accepts = accept.split(",");
		if (accepts.length > 1 || accepts.length == 0) {
			return false;
		}
		if (!(accepts[0].indexOf("application/json") >= 0)) {
			return false;
		}
		return true;
	}//:

}/// ~
