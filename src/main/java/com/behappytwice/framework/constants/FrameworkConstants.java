package com.behappytwice.framework.constants;

import org.springframework.http.HttpStatus;

/**
 * framework에서 사용할 상수들을 정의한다. 
 * 
 * @author behap
 *
 */
public class FrameworkConstants {
	
	/**
	 * HttpSession에 로그인한 사용자의 정보를 넣고 뺄 때 사용하는 키 
	 */
	public static final String SESSION_SIGNIN_USER_KEY = "signin_user";
	/** ThreadLocal에 저장할 HttpServletRequest의 키 */
	public static final String THREAD_LOCAL_HTTP_REQUEST_KEY = "request";
	/** ThreadLocal에 저장할 HttpServletResponse의 키 */
	public static final String THREAD_LOCAL_HTTP_RESPONSE_KEY = "response";
	
	/**
	 * ResponseMessage의 status에 설정할 값이다. 서버에서 정상적으로 처리되었다는 의미로 200을 사용한다. 
	 */
	public static final int HTTP_STATUS_OK = HttpStatus.OK.value();
	/**
	 * ResponseMessage status에 설정할 값이다. 서버에서 내부 오류가 발생했다는 의미로 500을 사용한다. 
	 */
	public static final int HTTP_STATUS_INTERNAL_SERVER_ERROR = HttpStatus.INTERNAL_SERVER_ERROR.value();
	
	
	/** 세션 없음 코드 */
	public static final String NO_SESSION_CODE = "no-session";
	/** 세션이 없을 때 redirect할 URL */
	public static final String NO_SESSION_REDIRECT_URL = "/portal/portal";
	/**
	 * Locale 설정을 위해 Cookie에 설정할 쿠키 이름 
	 */
	public static final String COOKIE_LOCALE_KEY = "locale";
	
	/** Database를 구분하기 위해서 사용한다. */
	public static final String DB_TYPE = "db.type";
	
	/** SQL Logging을 위한 profile 이름 */
	public static final String PROFILE_LOG4JDBC_NAME = "log4jdbc";
	/** SQL logging을 위한 profile의 값 */
	public static final String PROFILE_LOG4JDBC_VALUE = "yes";
	
	
	/** JWT Token의 쿠키 이름 */
	public static final String TOKEN_COOKIE_NAME = "accessToken";
	/** JWT Token 저장할 사용자 정보의 Claim의 키 */
	public static final String TOKEN_USER_INFO_KEY = "userInfo";
}///~
