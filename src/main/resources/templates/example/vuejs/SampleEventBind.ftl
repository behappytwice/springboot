<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<#include "/framework/include/BootStrapCss.ftl">
</head>
<body>

<div id="app">
</div>

<script type="module" src="/example/vuejs/component/SampleEventBind.js"></script> 

<#include "/framework/include/CommonJavaScript.ftl">
</body>
</html>