package com.behappytwice.example.java.enums;

import org.junit.Test;

public class EnumTest {

	@Test
	public void test() {
		System.out.println(SampleBasicStatusEnum.ACTIVE);
		System.out.println(SampleBasicStatusEnum.ACTIVE.ordinal());
		System.out.println(SampleBasicStatusEnum.ACTIVE.name());
	}
	
	@Test
	public void test2() {
		System.out.println(SampleWhoEnum.KOREA.name());
	}
	
	@Test
	public void test3() {
		for(SampleWhoEnum who : SampleWhoEnum.values()) {
			System.out.println(who.toString());
		}
	}
	@Test
	public void test4() {
		SampleWhoEnum who = SampleWhoEnum.valueOf("AMERICA");
		System.out.println(who.name());
	}
	@Test
	public void test5() {
		System.out.println(SampleWhoEnum.AMERICA.getCountry());
	}
	
	
	@Test
	public void test6() {
		System.out.println(SampleErrorEnum.USER_NOT_FOUND);
		System.out.println(SampleErrorEnum.USER_NOT_FOUND.name());
		SampleErrorEnum err = SampleErrorEnum.USER_NOT_FOUND;
		System.out.println(err.getReasonPhrase());
		System.out.println(err.getValue());
		SampleErrorEnum err2 = SampleErrorEnum.valueOf("USER_NOT_FOUND");
		System.out.println(err2);
	}
}
