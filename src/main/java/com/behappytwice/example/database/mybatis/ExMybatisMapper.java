package com.behappytwice.example.database.mybatis;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ExMybatisMapper {
	ExMybatisBean selectTest();
	int insert(ExMybatisBean bean);
}///~
