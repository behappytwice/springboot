import Ajax from '/framework/js/Ajax.js';
import EventBus from '/common/js/EventBus.js';
import BoardLeft from './BoardLeft.js';
import BoardRight from './BoardRight.js';
import BoardContent from './BoardContent.js';
import BoardTop from './BoardTop.js';

new Vue({
  el: '#app',
  template : `
	     <div> 
	        <b-container>
	           <b-row>
	               <b-col>
	                   <board-top></board-top>
	                   <hr>
	               </b-col>
	           </b-row>
	           <b-row>
	               <b-col sm="3">
	                <board-left @EVTC_MENU_CLICKED="onLeftMenuClicked"></board-left>
	               </b-col>
	               <b-col sm="6">
	                 <board-content></board-content>
	               </b-col>
	               <b-col sm="3">
	                 <board-right></board-right>
	               <b-col>
	           </b-row>
	           
            </b-container>
         </div>
  `,
  components: {
	  BoardTop:BoardTop,
	  BoardLeft:BoardLeft,
	  BoardRight:BoardRight,
	  BoardContent:BoardContent
  },
  created() { 
	  //EventBus.$on('EVT_LEFT_MENU_CLICKED', this.onLeftMenuClicked);  
  },
  methods : {
	  onLeftMenuClicked : function(message) {
		  if(message == 'WRITE') {
			  //alert("Left Menu Clicked");
		  }
	  }
  }
});
