package com.behappytwice.framework.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 날짜/시간 처리를 편리하게 사용할 수 있는 메소드들을 제공한다.
 * @author behap
 *
 */
public class DateTimeUtils {
	
	/**
	 * 현재 시간을 UTC 시간으로 반환한다.
	 * 
	 * @return
	 *    ISO 형식으로 반환한다. 2019-09-25T05:24:40.833Z
	 *    
	 */
	public static String nowToUTCString() {
		//Instant instant = Instant.now();
		//return instant.toString();
		// 아래는 위와 동일한 결과
		OffsetDateTime utc = OffsetDateTime.now(ZoneOffset.UTC);
		return utc.toString();
	}//:
	
	
	/**
	 * System의 default zone id를 문자열로 되돌린다. 
	 * @return
	 * 		 Asia/Seoul
	 */
	public static String systemDefaultZoneIdToString() {
		return ZoneId.systemDefault().getId();
	}//:
	
	/** 
	 * 현재 시간을 시스템 Default LocalDateTime으로 반환한다. 
	 * @return
	 * 		LocalDateTime
	 */
	public static LocalDateTime nowToSystemDefaultLocalTime() {
		Instant instant = Instant.now();
		return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
	}//:
	
	/**
	 * 특정 타임존의 시간대로 날짜시간을 반환한다. 
	 * @param instant 날짜시간 인스턴스
	 * @param zoneId  타임존 인스턴스
	 * @return
	 * 		LocalDateTime 인스턴스 
	 */
	public static LocalDateTime toLocalDateTime(Instant instant, ZoneId zoneId) {
		return LocalDateTime.ofInstant(instant, zoneId);
	}//:
	
	/**
	 * Instant를 Date로 변환한다.
	 * @param i
	 * @return
	 */
	public static Date toDate(Instant i) {
		return Date.from(i);
	}///:
	/**
	 * Date를 Instant로 변환한다. 
	 * @param d
	 * @return
	 */
	public static Instant toInstant(Date d) {
		return d.toInstant();
	}//:
	
	
	/**
	 * 특정 시간대의 ZoneDateTime 객체를 반환한다.
	 * @param instant  Instant 인스턴스 
	 * @param zoneId  ZoneId 인스턴스 
	 * @return
	 */
	public static ZonedDateTime toZonedDateTime(Instant instant, ZoneId zoneId) {
		return instant.atZone(zoneId);
	}//:
	
	/**
	 * 특정시간대의 시간 값으로 UTC와의 시간차이를 구한다.
	 * @param zonedDateTime  ZonedDateTime 인스턴스 
	 * @return
	 * 		시간차이 
	 */
	public static int timeDiff(ZonedDateTime zonedDateTime) {
		// UTC+0과의 시간차이를 구한다. 
		ZoneOffset offset = zonedDateTime.getOffset();
		System.out.println(offset.getTotalSeconds());
		// UTC하고 초단위로 시간차리를 구하고
		int seconds = offset.getTotalSeconds();
		// 시간으로 환산 
		int diff = seconds/(60 * 60);
		return diff;
	}//:
	
	/**
	 * ISO 형식으로 만들어진 문자열을 LocalDateTime으로 변환한다.
	 * @param isoString ISO 형식의 날짜시간 문자열
	 * @return
	 * 	   LocalDateTime 객체 
	 */
	public static LocalDateTime fromISOStringToLocalDateTime(String isoString) {
		  // JavaScript에서 JSON.stringify(date)로 만들어지는 ISO 형식의 문자열
        // 2014-01-01T13:13:34.441Z
        //String dateString = "2014-01-01T13:13:34.441Z";
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        LocalDateTime formatDateTime2 = LocalDateTime.parse(isoString, formatter2);
        return formatDateTime2;
	}//:
	
	/**
	 * LocalDateTime 값을 ISO 형식의 문자열로 변환 
	 * @param localDateTime LocalDateTime 객체
	 * @return
	 * 		 ISO 형식으로 변환된 문자열 
	 */
	public static String fromLocalDateTimeToISOString(LocalDateTime localDateTime) {
		String formatString = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatString);
        String formatDateTime = localDateTime.format(formatter);
        return formatDateTime;
	}//:
	
}///~
