package com.behappytwice.example.database.jdbctemplate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.behappytwice.example.model.ExUserBean;

/** JdbcTemplate를 테스트하기 위한 DAO */
@Repository
public class ExJdbcTemplateDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/** 모든 데이터를 가져온다. */
	public List<ExUserBean> listForBeanPropeprtyRowMapper() {
		String query = "SELECT * FROM be_test";
		return jdbcTemplate.query(query, 
				 new BeanPropertyRowMapper<ExUserBean>(ExUserBean.class));
	}
	
}///~
