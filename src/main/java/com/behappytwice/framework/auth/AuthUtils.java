package com.behappytwice.framework.auth;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.behappytwice.framework.constants.FrameworkConstants;
import com.behappytwice.framework.util.web.CookieUtils;
import com.behappytwice.framework.web.token.JavaTokenUtils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

/** 
 * 인증을 처리하기 위한 유틸리티 클래스이다. 
 * @author behap
 *
 */
public class AuthUtils {
	
	/** 
	 * Token이 유효한지 체크하고 토큰이 유효하면 토큰을 갱신합니다. 
	 * @param request
	 * @param reponse
	 * @return
	 */
	public static boolean checkToken(HttpServletRequest request, HttpServletResponse response) {
		// Token 유효성 검사
		boolean isTokenValid = false;
		String jwsStr = CookieUtils.getCookieValue(request, FrameworkConstants.TOKEN_COOKIE_NAME);
		if (jwsStr == null) {
			System.out.println("■■■■■■ Token not exist.");
		} else {
			try {
				Jws<Claims> jws = JavaTokenUtils.readToken(jwsStr);
				refreshToken(request, response, jws);
				System.out.println("■■■■■■ 유효한 토큰입니다.");
			}catch(Exception e) {
				isTokenValid = true;
				System.out.println("■■■■■■ 유효하지 않은 토큰입니다.");
			}
		}
		return true; 
	}//:
	
	
	/**
	 * Token을 새로 생성하고 Cookie에 다시 담습니다. 
	 * 
	 * @param request HttpServletRequest 인스턴스 
	 * @param response HttpServletResponse 인스턴스 
	 * @param jws Jws 인스턴스 
	 */
	public static void refreshToken(HttpServletRequest request, HttpServletResponse response, Jws<Claims> jws ) {
		String userJson = (String)JavaTokenUtils.getClaim(jws, FrameworkConstants.TOKEN_USER_INFO_KEY);
		long expiration = 60 * 60 * 24 * 1000;
		Map<String, String> claims = new HashMap();
		claims.put(FrameworkConstants.TOKEN_USER_INFO_KEY, userJson);
		String jwsString = JavaTokenUtils.createJws("behappytwice", "all", expiration, claims);
		CookieUtils.deleteCookie(response, FrameworkConstants.TOKEN_COOKIE_NAME);
		CookieUtils.addCookie(response, FrameworkConstants.TOKEN_COOKIE_NAME, jwsString);
	}//:
	
}///~
