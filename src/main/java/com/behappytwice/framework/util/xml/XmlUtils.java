package com.behappytwice.framework.util.xml;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXB;

import com.thoughtworks.xstream.XStream;

/**
 * XML을 편리하게 다룰 수 있는 메소드들을 제공한다. 
 * @author behap
 *
 */
public class XmlUtils {
	
	/**
	 * 객체를 XML로 반환한다. 
	 * @param clazz 변환할 객체의 타입 
	 * @param o 변환할  객체 
	 * @return
	 * 		XML 문자열 
	 */
	public static String marshal(Class clazz, Object o) {
		StringWriter sw = new StringWriter();
		JAXB.marshal(o, sw);
		return sw.toString();
	}// :

	/**
	 * XML을 자바 객체로 변환한다. 
	 * 
	 * @param xml XML 문자열 
	 * @param clazz 변환할 타입
	 * @return
	 * 		자바 객체 
	 */
	public static Object unmarshal(String xml, Class clazz) {
		return JAXB.unmarshal(new StringReader(xml), clazz);
	}// :

	
	/**
	 * 객체를 XML로 변환한다. 
	 * @param alias 루트 요소의 이름
	 * @param clazz Class
	 * @param o 변환할 객체 
	 * @return
	 * 		XML
	 */
	public static String toXML(String alias, Class clazz, Object o) {
		XStream xstream = new XStream();
		xstream.alias(alias, clazz);
		return xstream.toXML(o);
	}// :

	/**
	 * XML을 객체로 변환한다.
	 * @param alias root element의 이름
	 * @param clazz Class
	 * @param xml XML
	 * @return
	 * 		클래스의 인스턴스 
	 */
	public static Object readXmlToObjectTest(String alias, Class clazz, String xml) {
		XStream xstream = new XStream();
		xstream.alias(alias, clazz);
		return xstream.fromXML(xml);
	}
	
}///~
