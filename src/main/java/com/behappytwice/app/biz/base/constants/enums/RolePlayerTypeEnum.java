package com.behappytwice.app.biz.base.constants.enums;


public enum RolePlayerTypeEnum {
	MEMBER("M"),
	ADMIN("A"),
	GROUP("G");
	
	private final String rolePlayerType;
	
	public String getRolePlayerType() { 
		return this.rolePlayerType;
	}
	
	RolePlayerTypeEnum(String rolePlayerType) {
		this.rolePlayerType = rolePlayerType;
	}
}
