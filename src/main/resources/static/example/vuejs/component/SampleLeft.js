import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';

export default {
  // Left Navitaion Component
  // view section 
  // a tag 클릭했을 때 파라미터 전달 방법 https://stackoverflow.com/questions/43529516/how-to-pass-a-parameter-to-vue-click
  // @click="onMenuClicked(item.url)"
  template: `
     <div class="left">
         <ul id="menus">
             <li v-for="item in menus">
                <a href="#" @click="onMenuClicked(item.url)"> {{item.name}}</a>
             </li>
         </ul>
     </div>
  `,
  // constructor section 
  created() {
	  this.getMenus();
  },
  // data section 
  data() {
    return {
    	menus:[]
    }
  },
  // method section 
  methods: {
	  getMenus:function() {
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  url : '/vue/getMenus',
				  success : function(response) {
					  self.menus = response.data; // 저장해 놓은 self 사용
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Utils().ajax(requestConfig);
	  },
	  onMenuClicked: function(url) {
		  //alert("a menu clicked");
		  EventBus.$emit('EVENT_LEFT_MENU_CLICKED', url);
	  }
  }
} //export 