package com.behappytwice.framework.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.behappytwice.framework.util.json.JSONUtils;

/**
 * Root Application context 설정 클래스이다. 
 * 
 * @author behap
 *
 */
@Configuration
public class AppConfig {


	public AppConfig() {
	}
	
	/**
	 * ResourceBundleMessageSource 빈을 생성한다. i18n.json 파일, i18n-custom.json 파일에 설정된 다국어 리소스 
	 * properties 파일들을 읽어 들인다.  
	 * 
	 * @return
	 * 		ResourceBundleMessageSource 객체 
	 */
	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		try {

			List<String> list = JSONUtils.toList("i18n.json", String.class);
			try { 
				List<String> list2 = JSONUtils.toList("i18n-custom.json", String.class);
				list.addAll(list2);
			}catch(Exception e) {
				// ignores
			}

			String[] messageList = new String[list.size()];
			messageList = list.toArray(messageList);
			source.setBasenames(messageList);
			// source.setBasename(env.getRequiredProperty("message.source.basename"));
			source.setUseCodeAsDefaultMessage(true);
		} catch (Exception e) {
			// Ignores exceptions
		}
		return source;

	}// :

}/// ~
