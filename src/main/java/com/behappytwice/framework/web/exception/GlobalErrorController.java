package com.behappytwice.framework.web.exception;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Controller에 의해서 처리되지  않는 에러를 처리한다.
 *  
 * @author behap
 *
 */
@Controller
public class GlobalErrorController implements ErrorController {
	
	@Override
	public String getErrorPath() {
		// 에러경로가 어디에 있다는 것을 알려주기위한 용도로 보임
		// 실제 경로를 사용하지 않음 
		return "/error";
	}
	
	@RequestMapping(value="/error")
	public String handleError(HttpServletRequest request) {
	    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
	    if (status != null) {
	        Integer statusCode = Integer.valueOf(status.toString());
	        if(statusCode == HttpStatus.NOT_FOUND.value()) {
	        	//src/main/resources/template/error 아레에 있는 error-404.ftl
	            return "error/error-404";
	        }
	        else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
	        	//src/main/resources/template/error 아레에 있는 error-500.ftl
	            return "error/error-500";
	        }
	    }
	    return "error/error";

	}//:
	
}///~

		