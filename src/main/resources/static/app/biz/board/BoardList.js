import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';

export default {
  template: `
     <div>
						 <b-card  v-for="item in articles"
						    v-bind:title="item.subject"
						    img-src="https://picsum.photos/600/300/?image=25"
						    img-alt="Image"
						    img-top
						    tag="article"
						    style="max-width: 100rem;"
						    class="mb-2"
						  >
						    <b-card-text>
						      {{item.content}}
						    </b-card-text>
						    <b-button href="#" variant="primary">Go somewhere</b-button>
						  </b-card>	              
     </div>
  `,
  // constructor section 
  created() {
	  this.findAllArticles();
  },
  // data section 
  data() {
    return {
    	articles:[]
    }
  },
  // method section 
  methods: {
	  findAllArticles: function() {
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  url : '/board/article/BD_ATCL_100_FIND_ALL_ARTICLES',
				  success : function(response) {
					  self.articles = response.data; // 저장해 놓은 self 사용
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Utils().ajax(requestConfig);
	  }
  }
} //export 