package com.behappytwice.framework.json;


import java.util.List;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;


/**
 * String 타입의 필드가 null이면 NullStringSerializer를 반환하고 List 타입의 필드가 널이면 
 * NullListSerializer를 반환한다.
 * 
 * @author Sanghyun,Kim(sanghyun@naonsoft.com)
 *
 */
public class CustomSerializerProvider extends DefaultSerializerProvider {

    public CustomSerializerProvider() {
        super();
    }

    public CustomSerializerProvider(CustomSerializerProvider provider, SerializationConfig config,
            SerializerFactory jsf) {
        super(provider, config, jsf);
    }

    @Override
    public CustomSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
        return new CustomSerializerProvider(this, config, jsf);
    }

    @Override
    public JsonSerializer<Object> findNullValueSerializer(BeanProperty property) throws JsonMappingException {
    	
        if (property.getType().getRawClass().equals(String.class)) {
            return NullStringSerializer.EMPTY_STRING_SERIALIZER_INSTANCE;
        }else if (property.getType().getRawClass().equals(List.class)) {
            return NullListSerializer.EMPTY_LIST_SERIALIZER_INSTANCE;
        }
        else { 
            return super.findNullValueSerializer(property);
        }
    }
}