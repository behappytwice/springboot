package com.behappytwice.example.database.jdbctemplate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.behappytwice.framework.base.component.BaseController;
import com.behappytwice.example.model.ExUserBean;


/** JdbcTemplate를 테스트하기 위한 컨트롤러이다. */
@Controller
@RequestMapping(value="/example/jdbc")
public class ExJdbcTemplateController extends BaseController {

	@Autowired
	private ExJdbcTemplateDAO bcTestDAO;
	
	/** 사용자를 가져온다. */
	@RequestMapping(value="/getUser")
	public String getUser() throws Exception {
		List<ExUserBean> list = bcTestDAO.listForBeanPropeprtyRowMapper();
		if(list == null) {
			logger.debug("The list is null.");
		}
		for(ExUserBean bean : list) {
			System.out.println(bean.getUserName());
		}
		return "index";
	}//:
	
}///~
