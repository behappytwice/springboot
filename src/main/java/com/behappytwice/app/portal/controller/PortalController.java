package com.behappytwice.app.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.behappytwice.app.biz.organization.entity.Member;
import com.behappytwice.app.portal.service.PortalService;
import com.behappytwice.framework.base.component.BaseController;
import com.behappytwice.framework.web.message.ResponseMessage;
import com.behappytwice.framework.web.session.SessionUtils;

@Controller
@RequestMapping(value="/portal")
public class PortalController extends BaseController {
	
	@Autowired
	private PortalService portalService; 
	
	@RequestMapping(value="/portal")
	public String portal(HttpServletRequest request) {
		return "app/portal/Portal";
	}//:
	
	@RequestMapping(value="/insertMember")
	public @ResponseBody ResponseMessage insertMember(@RequestBody Member member) {
		this.portalService.insertMember(member);
		return this.createSuccessMessage(member);
	}
	
	@RequestMapping(value="/findByEmail")
	
	public @ResponseBody ResponseMessage findByEmail(@RequestBody Member member) {
		this.logger.debug(">>>>>>>>>>>>>>>>>> findByEmail");
		this.logger.debug(">>> email:" + member.getEmail());
		Member member2 = this.portalService.findByEmail(member.getEmail());
		return this.createSuccessMessage(member2);
	}//;
	
	@RequestMapping(value="/signin")
	public @ResponseBody ResponseMessage signIn(@RequestBody Member member) {
		logger.debug(">>>>> member");
		logger.debug(member.getPassword());
		Member retMember = this.portalService.signIn(member);
		return this.createSuccessMessage(retMember);
	}//:
	
	
}////~
