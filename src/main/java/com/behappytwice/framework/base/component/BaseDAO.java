package com.behappytwice.framework.base.component;

import com.behappytwice.framework.base.BaseBean;

/**
 * 모든 DAO 클래스의 슈퍼클래스이다. DAO 클래스를 구현할 때 이 클래스를 상속 받아야 한다. 
 * @author behap
 *
 */
public class BaseDAO extends BaseBean {

}////~
