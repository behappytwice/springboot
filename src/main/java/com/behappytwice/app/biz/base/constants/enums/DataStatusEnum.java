package com.behappytwice.app.biz.base.constants.enums;

public enum DataStatusEnum {

	DELETED("D"), CREATED("C"),MODIFIED("M");
	
	private String status;
	
	DataStatusEnum(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}
}///~
