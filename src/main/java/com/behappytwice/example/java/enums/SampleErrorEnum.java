package com.behappytwice.example.java.enums;

/** Map 형식의 Enum으로 코드와 값을 반환하는 두 개의 getter를 구현해야 한다.*/
public enum SampleErrorEnum {
	
	USER_NOT_FOUND(901, "User not found" ),
	INTERNAL_ERROR(500, "Internal error");
	
	private final int value; 
	private final String reasonPhrase;
	
	public int getValue() {
		return this.value;
	}
	public String getReasonPhrase() {
		return this.reasonPhrase;
	}
	
	SampleErrorEnum(int value, String reasonPhrase) {
		this.value = value;
		this.reasonPhrase = reasonPhrase;
	}
}
