package com.behappytwice.framework.web.interceptor;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.behappytwice.framework.base.BaseBean;

//@InterceptorAnnotation(addPathPattern = { "/**" }, excludePathPattern = {})
public class TestInterceptor extends BaseBean implements HandlerInterceptor  {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		//System.out.println("★★★★★★★ TestInterceptor");
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		Locale locale =  RequestContextUtils.getLocale(request);
		System.out.println("Language="  +locale.getLanguage());
		
		return true;
		
	}//:~

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
	
	
}////~
