package com.behappytwice.biz.board.service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.function.Function;

import org.junit.Test;

import com.behappytwice.app.biz.board.entity.Article;
import com.behappytwice.framework.util.ReflectionUtils;
import com.behappytwice.framework.util.web.HtmlFormUtils;


public class ArticleServiceTestCase {

	@Test
	public void test() {
		
		/*
		Field[] fields = Article.class.getDeclaredFields();
		if(fields == null) return;
		for(Field fld : fields) {
			System.out.println(fld.getName());
		}
		System.out.println("==============================================================");
		*/
	
		// https://www.baeldung.com/java-reflection-class-fields
		List<Field> fieldList = ReflectionUtils.getAllFields(Article.class);
		fieldList.forEach(item-> System.out.println(item.getName() ));
		
		
		
		String formHtml = HtmlFormUtils.makeForm(Article.class);
		System.out.println("========================================");
		System.out.println(formHtml);
		
		
	}
	
}
