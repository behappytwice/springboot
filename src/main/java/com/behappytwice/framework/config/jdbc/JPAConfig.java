package com.behappytwice.framework.config.jdbc;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * Hibernate을 이용한 JPA Configuration 클래스이다. 하나 이상의 데이터 소스를 사용해야 하는 경우는
 * 이 클래스와 같은 Configuration class를 생성해서 사용해야 한다. EntityManagerFactory, DataSource,
 * PlatformTransactionManager 빈을 생성할 때 name 속성에 이름을 주어 생성한다.   {@literal @}Transactional
 * 어노테이션을 사용할 때 이름을 사용하여 어느 트랜잭션 매니저를 사용해야 하는지 명확히 해야 한다.
 * 
 * {@literal}EnableJpaRepositories를 설정할 때 EntityManagerFactory에 대한 참조와 TransactionManager
 * 에 대한 참조를 설정해야 한다. 
 * 
 * @author behap
 *
 */
@Configuration
@EntityScan(basePackages = { "com.behappytwice" })
@EnableJpaRepositories(
		 basePackages = { "com.behappytwice" },
		 entityManagerFactoryRef="entityManagerFactory",
		 transactionManagerRef="hibernateTransactionManager"
)
@EnableTransactionManagement
public class JPAConfig {
	
	@Autowired
	Environment environment;

	/**
	 * EntityManagerFactory를 생성한다.
	 * 
	 * 'entityManagerFactory'라는 이름으로 빈을 생성하면 Spring Boot auto-configuration이
	 * 그것의 entity manager를 스위치 오프(switch off)한다. 
	 * 
	 * @return
	 * 
	 */
	@Bean( name="entityManagerFactory")
	@Primary
	public EntityManagerFactory entityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.behappytwice");
		factory.setDataSource(hibernateDataSource());
		factory.afterPropertiesSet();

		return factory.getObject();
	}//:
	
	
	/**
	 * DataSource를 생성한다.
	 * applicaiton.properties에 설정된 properties들을 읽어들여 설정한다. 
	 * {@literal @}ConfigurationProperties의 prefix 속성에 값을 설정하면
	 * 그 값을 제외한 이하 속성이 설정된다.
	 * 
	 * @return
	 */
	@Bean(name = "hibernateDataSource")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource.hikari")
	public DataSource hibernateDataSource() {
		return DataSourceBuilder.create().build();
	}//:

	/**
	 * PlatformTransactionManager를 생성한다.
	 * 
	 * @return
	 */
	@Primary
	@Bean(name = "hibernateTransactionManager")
	public PlatformTransactionManager hibernateTransactionManager() {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory());
		return txManager;
	}//:
	
}/// ~
