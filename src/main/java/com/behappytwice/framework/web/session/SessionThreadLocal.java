package com.behappytwice.framework.web.session;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.behappytwice.framework.constants.FrameworkConstants;

public class SessionThreadLocal {
	
	private static ThreadLocal<Map> threadLocal = new ThreadLocal<Map>();
	
	/** Map을 설정한다.*/
	public static void setValue(Map m) {
		threadLocal.set(m);
	}//:
	/** Map을 반환한다.*/
	public static Map getValue() {
		return threadLocal.get();
	}//:
	
	/** HttpServletRequest 를 반환한다. */
	public static HttpServletRequest getServletRequest() {
		Map map = threadLocal.get();
		return (HttpServletRequest)map.get(FrameworkConstants.THREAD_LOCAL_HTTP_REQUEST_KEY);
	}//:
	
	/** HttpServletResponse를 반환한다.*/
	public static HttpServletResponse getServletResponse() {
		Map map = threadLocal.get();
		return (HttpServletResponse)map.get(FrameworkConstants.THREAD_LOCAL_HTTP_RESPONSE_KEY);
	}//:
	
	
}///~
