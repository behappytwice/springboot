package com.behappytwice.framework.config.util;

import java.util.List;

import org.junit.Test;

import com.behappytwice.framework.util.json.JSONUtils;
import com.behappytwice.framework.web.interceptor.InterceptorConfig;

public class AppConfigUtilTest {

	@Test
	public void test() {
		String path = "interceptor.json";
		try {
			
			List<InterceptorConfig> list = JSONUtils.toList(path, InterceptorConfig.class);
			
			for(InterceptorConfig config : list) {
				System.out.println(config.getName());
			}//:
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Test
	public void test2() {
		String path = "i18n.json";
		try {
			
			List<String> list = JSONUtils.toList(path, String.class);
			
			for(String str: list) {
				System.out.println(str);
			}//:
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
