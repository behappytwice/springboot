package com.behappytwice.framework.base;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.behappytwice.framework.log.Log;


/**
 * Spring Bean으로 사용될 모든 클래스의 슈퍼 클래스이다. BaseController, BaseDAO, BaseService 클래스는
 * 이 클래스를 상속받아서 구현했다. 
 * 
 * @author behap
 *
 */
@Component
public class BaseBean {
	
	/** 현재 어플리케이션이 실행되고 있는 환경 */
	@Autowired
	protected Environment env;
	/** Logger */
	protected static @Log Logger logger;
	
}///~
