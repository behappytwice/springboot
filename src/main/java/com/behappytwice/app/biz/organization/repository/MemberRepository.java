package com.behappytwice.app.biz.organization.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.behappytwice.app.biz.organization.entity.Member;


public interface MemberRepository extends JpaRepository<Member, String> {
	
	Member findByEmail(String email);
	Member findByName(String name);

}////~
