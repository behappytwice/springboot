package com.behappytwice.example.java.lang;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class LangTest {
	
	
	@Test
	public void test() {
		/// Arrays.asList() 사용 
		Integer[] spam = new Integer[] { 1, 2, 3 };
		List<Integer> list = Arrays.asList(spam);
		list.forEach( el -> System.out.println(el) );
	}//:
	

}///~
