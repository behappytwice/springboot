package com.behappytwice.framework.web.interceptor;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.behappytwice.framework.constants.FrameworkConstants;
import com.behappytwice.framework.util.web.CookieUtils;

public class AllRequestInterceptor implements HandlerInterceptor  {

	@Autowired
	private LocaleResolver localeResolver;
	@Autowired
    private MessageSource messageSource;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		
		// Cookie에 locale이 설정되어 있지 않으면 브라우저의 Accept-Langauge의 첫번째 언어로 로케일을 설정한다.
		// locale이 ko가 아니면 en으로 설정한다.
		// https://help.localizejs.com/docs/detecting-language-of-a-visitor
		
		String hederValue = request.getHeader("Accept-Language");
		System.out.println("Accept-Language:" + hederValue);

		String acceptLang = null; 
		String[] acceptLanguages = hederValue.split(",");
		if(acceptLanguages != null && acceptLanguages.length > 0) {
			String firstLang = acceptLanguages[0];
			String[] strippedLang = firstLang.split(";");
			acceptLang = strippedLang[0];
		}
		if(!acceptLang.equals("ko")) {
			acceptLang = "en";
		}
		
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		// 언어를 변경하지 않으면 cookie에 locale 이 없다
		String cookieValue = CookieUtils.getCookieValue(request, FrameworkConstants.COOKIE_LOCALE_KEY);
		if(cookieValue == null) {
			Locale newLocale = new Locale(acceptLang);
			localeResolver.setLocale(request, response, newLocale);
			CookieUtils.addCookie(response, FrameworkConstants.COOKIE_LOCALE_KEY, acceptLang);
		}

		//System.out.println("Message : " + this.messageSource.getMessage("user.name",null, RequestContextUtils.getLocale(request)));
		
		
		return true;
	}//:
	
}
