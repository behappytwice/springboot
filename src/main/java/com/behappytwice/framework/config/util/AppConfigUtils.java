package com.behappytwice.framework.config.util;

import java.util.Arrays;

import org.springframework.core.env.Environment;


/**
 * Configuration class에서 사용할 편리한 메소드들을 제공한다.  
 * @author behap
 *
 */
public class AppConfigUtils {

	
	/**
	 * 프로파일이 존재하는지 확인한다. 
	 * 
	 * @param env  Environment 객체 
	 * @param profile 프로파일 이름 
	 * @return
	 * 		주어진 프로파일이름이 있으면 true, 없으면 false를 반환한다. 
	 */
	public static boolean isProfileExists(Environment env, String profile) {
		if (Arrays.stream(env.getActiveProfiles()).anyMatch(envl -> (envl.equalsIgnoreCase(profile)))) {
			return true;
		}
		return false;
	}// :


}/// ~
