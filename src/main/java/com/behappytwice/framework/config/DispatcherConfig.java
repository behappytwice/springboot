package com.behappytwice.framework.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import com.behappytwice.framework.constants.FrameworkConstants;
import com.behappytwice.framework.json.CustomObjectMapper;
import com.behappytwice.framework.json.CustomSerializerProvider;
import com.behappytwice.framework.log.Log;
import com.behappytwice.framework.util.json.JSONUtils;
import com.behappytwice.framework.web.interceptor.InterceptorConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Web Application Context configuration class이다. Spring 5 버전을 기준으로 작성되었다. 
 * SpringBoot에서 WebMvcConfigurer를 구현하는 경우에는  addResourceHandlers()를 override해야 한다.
 * 
 * @author behap
 *
 */
@EnableWebMvc
@Configuration
public class DispatcherConfig implements WebMvcConfigurer  {

	
	/** properties 파일에 정의된 설정값들이 담겨 있다.*/ 
	private Environment env;
	
	@Autowired
	public DispatcherConfig(Environment env) {
		this.env = env; 
	}
	
	/** Logger */
	protected static @Log Logger logger;
	@Autowired
	private ApplicationContext applicationContext;
	/** 클라이언트가 정적자원을 요청할 때 정적자원의 Spring이 처리할 정적자원의 위치를 설정한다. */
	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
			"classpath:/META-INF/resources/", "classpath:/resources/",
			"classpath:/static/", "classpath:/public/" };
	
	/**
	 * 이 메소드를 구현하지 않으면 No Mapping for ~ 오류가 발생함.  
	 * 
	 * https://stackoverflow.com/questions/30406186/spring-boot-java-config-no-mapping-found-for-http-request-with-uri-in
	 * 
	 */
//	@Override
//    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//        configurer.enable();
//    }//:
	
	
	/**
	 * https://spring.io/blog/2013/12/19/serving-static-web-content-with-spring-boot
	 * 
	 * Tomcat의 서블릿 핸들러를 사용하는 경우에는 src/main/resources/static에 있는 js파일들을 못찾음
	 * 그래서 static 폴더의 경로를 지정해야 함.  
	 * 
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		if (!registry.hasMappingForPattern("/webjars/**")) {
			registry.addResourceHandler("/webjars/**").addResourceLocations(
					"classpath:/META-INF/resources/webjars/");
		}
		if (!registry.hasMappingForPattern("/**")) {
			registry.addResourceHandler("/**").addResourceLocations(
					CLASSPATH_RESOURCE_LOCATIONS);
		}
	}
	
	
	
	/**
	 * interceptor.json 파일에 정의된 인터셉터들을 읽어서 스프링 빈으로 등록한다. 
	 * Note : 인터셉터는 등록된 순서대로 실행된다. 
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		logger.info("인터셉터를 등록 중입니다.");

		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		try {
			List<InterceptorConfig> list = new ArrayList<InterceptorConfig>();
			try {
				List<InterceptorConfig> bean = JSONUtils.toList("interceptor.json",	InterceptorConfig.class);
				if (bean != null) {
					list.addAll(bean);
				}
			} catch (Exception e) {
				// Ignores this error
			}
			try {
				List<InterceptorConfig> bean = JSONUtils.toList("interceptor-custom.json",
						InterceptorConfig.class);
				if (bean != null) {
					list.addAll(bean);
				}

			} catch (Exception e) {
				// Ignores this error
			}

			
			// if the size of the list is zero, there is nothing to do.
			if (list == null || list.size() == 0)
				return;

			InterceptorConfig[] interceptorList = new InterceptorConfig[list.size()];
			interceptorList = list.toArray(interceptorList);
			ClassLoader classLoader = this.getClass().getClassLoader();

			for (InterceptorConfig configBean : interceptorList) {
				// Loads a interecptor
				Class aClass = classLoader.loadClass(configBean.getName().trim()); // class name
				// Creates a interceptor
				AutowireCapableBeanFactory factory = this.applicationContext.getAutowireCapableBeanFactory();
				BeanDefinitionRegistry beanRegistry = (BeanDefinitionRegistry) factory;
				GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
				beanDefinition.setBeanClass(aClass);
				beanDefinition.setAutowireCandidate(true);
				beanRegistry.registerBeanDefinition(aClass.getSimpleName(), beanDefinition);
				factory.autowireBeanProperties(this, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);

				// InterceptorRegistration regis = registry
				// .addInterceptor((HandlerInterceptor) aClass.newInstance());
				InterceptorRegistration regis = registry
						.addInterceptor((HandlerInterceptor) this.applicationContext.getBean(aClass.getSimpleName()));

				if (configBean.getAddPathPattern() != null && configBean.getAddPathPattern().length > 0) {
					regis.addPathPatterns(configBean.getAddPathPattern());
				}
				if (configBean.getExcludePathPattern() != null && configBean.getExcludePathPattern().length > 0) {
					regis.excludePathPatterns(configBean.getExcludePathPattern());
				}
				

//				Annotation[] annotations = aClass.getAnnotations();
//				for (Annotation annotation : annotations) {
//					if (annotation instanceof InterceptorAnnotation) {
//						InterceptorAnnotation intercAnno = (InterceptorAnnotation) annotation;
//						String[] addPathPattern = intercAnno.addPathPattern();
//						String[] excludePathPattern = intercAnno.excludePathPattern();
//						// registry.addInterceptor(new
//						// SampleInterceptor()).addPathPatterns("/sample/**");
//						if (addPathPattern != null && addPathPattern.length > 0) {
//							for (String pattern : addPathPattern) {
//								regis.addPathPatterns(pattern);
//							}
//						}
//						if (excludePathPattern != null && excludePathPattern.length > 0) {
//							for (String pattern : excludePathPattern) {
//								regis.excludePathPatterns(pattern);
//							}
//						}
//					}
//				} // for

				logger.info(configBean.getName().trim() + " 인터셉터를 등록했습니다.");
			} // for
			logger.info("모든 인터셉터를 등록했습니다.");
		} catch (Exception e) {
			// Ignores exceptions
			e.printStackTrace();
		}
		
	}//:
	
	
	
	/**
	 * CookieLocalResolver를 생성한다. 
	 * 
	 * @return
	 * 		CookieLocaleResolver 객체 
	 */
	@Bean
	public CookieLocaleResolver localeResolver() {
		CookieLocaleResolver localeResolver = new CookieLocaleResolver();
		localeResolver.setDefaultLocale(Locale.KOREA); // 기본 한국어
		localeResolver.setCookieName(FrameworkConstants.COOKIE_LOCALE_KEY); // 쿠키 이름 ; locale
		localeResolver.setCookieMaxAge(60 * 60); // 쿠키 살려둘 시간, -1로 하면 브라우져를 닫을 때 없어짐.
		localeResolver.setCookiePath("/"); // Path를 지정해 주면 해당하는 path와 그 하위 path에서만 참조
		return localeResolver;
	}//:
	
	
	/**
	 * MessageConverter를 등록한다.
	 * See https://www.baeldung.com/spring-httpmessageconverter-rest
	 */
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
		messageConverters.add(jacksonJsonConverter());
	}//:
	
	
	/**
	 * POJO를 JSON으로 JSON을 POJO로 변환하는 Message Converer를 생성한다.
	 * See https://www.baeldung.com/spring-httpmessageconverter-rest
	 * @return
	 *    MappingJackson2HttpMessageConverter 인스턴스
	 */
	@Bean
	MappingJackson2HttpMessageConverter jacksonJsonConverter() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setObjectMapper(objectMapper());
		List<MediaType> mediaTypes = new ArrayList<>();
		mediaTypes.add(MediaType.APPLICATION_JSON);
		mediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
		converter.setSupportedMediaTypes(mediaTypes);
		return converter;
	}//:
	
	/**
	 * Jackson ObjectMapper를 생성한다.
	 * @return
	 * 		ObjectMapper 인스턴스 
	 */
	@Bean
	public ObjectMapper objectMapper() {
		List<String> dateFormats = new ArrayList<String>();
		CustomObjectMapper mapper = new CustomObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setSerializerProvider(new CustomSerializerProvider());
		return mapper;
	}//:
	
}///~
