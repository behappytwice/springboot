package com.behappytwice.app.biz.base.constants.enums;

public enum EntryTypeEnum {

	/** 게시판 **/
	BOARD("BRD"),
	/** 알림 **/
	NOTICE("NOT");
	
	private final String entryType;
	
	public String getEntryrType() { 
		return this.entryType;
	}
	
	EntryTypeEnum(String entryType) {
		this.entryType = entryType;
	}
	
}///~
