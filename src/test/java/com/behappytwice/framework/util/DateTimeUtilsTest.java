package com.behappytwice.framework.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Set;

import org.junit.Test;

/** DateTimeUtils class의 테스트 케이스 */
public class DateTimeUtilsTest {

	/**
	 * 로컬의 현재 시간을 구한다. 
	 */
	@Test
	public void testCurrentDateTime() {
		LocalDateTime localDateTime  = LocalDateTime.now();
		System.out.println(localDateTime);
		// 2019-09-25T15:50:43.265
	}
	
	/**
	 * 현재시간을 UTC 시간으로 구한다.
	 * @return
	 */
	@Test
	public void testCurrentDateTimeUTC() {
		Instant instant = Instant.now();
		System.out.println(instant);
		//2019-09-25T06:52:31.296Z
	}
	/**
	 * 모든 ZoneId를 출력한다.
	 */
	@Test
	public void testPrintAllZoneIds() {
		Set<String> allZoneIds = ZoneId.getAvailableZoneIds();
		allZoneIds.forEach(id -> System.out.println(id));
	}

	/** System Default ZoneId를 구한다. */
	@Test
	public void testSystemDefault() {
		System.out.println("Defalut Zone ID: " + ZoneId.systemDefault());
	}
	/** 시간 값이 없는 로컬 날짜를 설정한다. */
	@Test
	public void testSetLocalDate() {
		LocalDate date = LocalDate.of(2002, 6, 18);
		System.out.println("Date = " + date);
	}
	/** 날짜 값이 없는 로컬 시간을 설정한다. */ 
	@Test 
	public void testSetLocalTime() {
		LocalTime time = LocalTime.of(20, 30);
		System.out.println("Time = " + time);
	}//:
	/** 특정 날짜와 시간으로 로컬 날짜를 설정한다.  */
	@Test
	public void testSetLocalDateTime() {
		LocalDateTime localDateTime = LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30);
		System.out.println(localDateTime);
	}
	/** 특정 날짜와 시간과 타임존으로 타임존 날짜를 설정한다.  */
	@Test
	public void testSetZonedDateTime() {
		LocalDateTime localDateTime = LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30);
		System.out.println(localDateTime);
		// 시간대가 변경되는 것은 아니고 특정 시간대의 날짜시간이라른 것을 설정한다.
		ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("Asia/Seoul"));
		System.out.println(zonedDateTime);
	}//:
	/** 특정 시간대의 날짜시간을 UTC 시간대로 변경한다. */
	@Test
	public void testZonedDateTimeToUTC() {
		LocalDateTime localDateTime = LocalDateTime.of(2015, Month.FEBRUARY, 20, 06, 30);
		System.out.println(localDateTime);
		// 시간대가 변경되는 것은 아니고 특정 시간대의 날짜시간이라른 것을 설정한다.
		ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("Asia/Seoul"));
		System.out.println(zonedDateTime);
		Instant instant = zonedDateTime.toInstant();
		System.out.println(instant);
	}//:
	
	
	/** 로컬 시간을 UTC로 변경 */
	@Test
	public void testZonedDateTimeToUTC2() {
		LocalDateTime dateTime = LocalDateTime.of(2019, Month.SEPTEMBER, 16, 00, 00);
		// 로컬시간에 타임존 설정
		ZonedDateTime seoulTime = dateTime.atZone(ZoneId.of("Asia/Seoul"));
		System.out.println(seoulTime);
		// 서울시간을 UTC+0/Z로 변환 , java.time이 9시간을 경감한다.
		Instant instant = seoulTime.toInstant();
		System.out.println(instant);
	}//:
	
	/** UTC 시간을 특정 타임존 시간으로 변경하고 시간차이를 구한다. */
	@Test
	public void testUTCtoZonedDateTime() {
		// Z = UTC+0 시간을 구한다. 
		Instant instant = Instant.now();
		System.out.println(instant);
		// 로컬시간에 타임존 설정

		//ZonedDateTime seoulTime = instant.atZone(ZoneId.of("Asia/Seoul"));
		ZonedDateTime seoulTime = instant.atZone(ZoneId.of("America/Montreal"));
		System.out.println(seoulTime);
		// UTC+0과의 시간차이를 구한다. 
		System.out.println(seoulTime.getOffset());
		ZoneOffset offset = seoulTime.getOffset();
		System.out.println(offset.getTotalSeconds());
		// UTC하고 초단위로 시간차리를 구하고
		int seconds = offset.getTotalSeconds();
		// 시간으로 환산 
		System.out.println(seconds/(60 * 60));
	}//:
	
	/** 현재시간을 UTC로 구하고 java.util.Date로 변환 */
	@Test
	public void testUTCToDate() {
		// http://abh0518.net/tok/?p=652
		// DB TimeZone에 대한 내용
		Instant instant = Instant.now();
		System.out.println(instant);
		//Date date1 = new Date(instant.toEpochMilli());
		//System.out.println(date1);
		Date date2 = Date.from(instant);
		// 출력하면 한국시간으로 보임 , DB에 넣을 때는 어떤지 확인해 보아야 함 
		System.out.println(date2);
	}//:
	
	/** java.util.Date를 Instant로 변환 */
	@Test
	public void testDateToInstant() {
		Instant instant = Instant.now();
		System.out.println(instant);
		Date date = Date.from(instant);
		System.out.println(date);
		Instant instant2 = date.toInstant();
		System.out.println(instant2);
	}//;
	
	/** 시간을 특정 포맷에 맞게 문자열로 변환 */
	@Test
	public void testDateTimeToString() {
		 //Get current date time
        LocalDateTime now = LocalDateTime.now();
        System.out.println("Before : " + now);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDateTime = now.format(formatter);
        System.out.println("After : " + formatDateTime);
      
	}//:
	/** 문자열을 LocalDateTime으로 변환 */
	@Test
	public void testStringToLocalDateTime() {

        String now = "2016-11-09 10:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime formatDateTime = LocalDateTime.parse(now, formatter);
        System.out.println("Before : " + now);
        System.out.println("After : " + formatDateTime);
        System.out.println("After : " + formatDateTime.format(formatter));

        // JavaScript에서 JSON.stringify(date)로 만들어지는 ISO 형식의 문자열
        // 2014-01-01T13:13:34.441Z
        String dateString = "2014-01-01T13:13:34.441Z";
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        LocalDateTime formatDateTime2 = LocalDateTime.parse(dateString, formatter2);
        System.out.println(formatDateTime2);
	}//:
	
}///~
