<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<script src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>

<script type="text/javascript">

    // https://stomp-js.github.io/stomp-websocket/codo/class/Client.html
    // https://github.com/sockjs/sockjs-client
    // http://jmesnil.net/stomp-websocket/doc/
    // Spring에서 Stomp헤더에 접근하는 방법(아래) 
    // https://medium.com/@hardeek.sharma/accessing-header-in-stomp-web-socket-connection-with-spring-boot-3d4a962a0f00
    
    var socket;
    var stompClient;
    var subscriptionDestination = "/topic/in";
    var sendDestination = "/app/in/tt";
    
    /** 초기화 */ 
    function init() {
       connect();
       document.getElementById("sendBtn").addEventListener("click", onClickButton);
       document.getElementById("message").addEventListener("keydown", function(event) {
          //console.log("evernt");
          if(event.which == 13) {
             document.getElementById("sendBtn").click();
             this.value = "";
          }  
       });
    }

    /** StompClient 생성 및 설정 */
    function connect() {
        // SockJS 생성 
        socket      = new SockJS('http://localhost:80//stompWebSocketHandler');  //websocket이아닌 SockJS로 접속한다.
        // StompClient 생성 
        stompClient = Stomp.over(socket); //stompClient에 socket을 넣어준다.
        stompClient.debug = function(str) {}; // diable debugging
        // 서버 연결 
        stompClient.connect({}, function() { //접속
            /** 구독 */
            stompClient.subscribe('/topic/in', function(msg) {
                var json = JSON.parse(msg.body);
                //alert(json.message);
                displayMessage(json.message);
            });
        },function(msg) {
        	// when disconenected
        	 alert("this has been disconnected.")
        });
    }
    
    
    /** 수신받은 메시지를 화면에 출력한다. */
    function displayMessage(message) {
        var output = document.getElementById("output");
        var pre = document.createElement("P");
		pre.style.wordWrap = "break-word";
		pre.innerHTML = message;
		output.appendChild(pre);
    }//:
    
    /** 서버로 메시지 전송 */
    function onClickButton() {
        var messageToSend = document.getElementById("message").value;
        var uId           = document.getElementById("userId").value;
        var uName         = document.getElementById("userName").value;
        var message = {
            userId   : uId,
            userName : uName,
            message  : messageToSend
        };
        stompClient.send('/app/in/tt', {}, JSON.stringify(message ) );   
    }
    
    window.addEventListener("load", init, false);

</script>
</head>
<body>
  <h2>Stomp 클라이언트 테스트 </h2>
  사용자 아이디 : <input type="text" id="userId"><br>
  사용자 이름   : <input type="text" id="userName"><br>
  
  메시지 : <input type="text" id="message">&nbsp;<input type="button" id="sendBtn" value="전송">
  <div id="output"></div>

</body>
</html>