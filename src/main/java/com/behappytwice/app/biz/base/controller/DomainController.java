package com.behappytwice.app.biz.base.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.behappytwice.app.biz.base.entity.RolePlayer;
import com.behappytwice.app.biz.base.service.RolePlayerService;

@Controller
@RequestMapping(value="/domain")
public class DomainController {
	@Autowired
	private RolePlayerService rolePlayerService;
	
	@RequestMapping(value="/addRolePlayer")
	public String addRoleplayer() {
		RolePlayer player = new RolePlayer();
		player.setName("박찬호");
		this.rolePlayerService.addRolePlayer(player);
		return "/";
		
	}
}///~
