package com.behappytwice.app.biz.organization.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.behappytwice.app.biz.base.entity.RolePlayer;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="member")
//@DiscriminatorValue("M")
//@PrimaryKeyJoinColumn(name = "rolePlayerID")
@Getter
@Setter
public class Member extends RolePlayer {

	@Column(name="self_description")
	@Getter @Setter
	private String selfDescription;
	@Column(name="email")
	private String email;
	@Column(name="password")
	private String password;
	
	
}///~
