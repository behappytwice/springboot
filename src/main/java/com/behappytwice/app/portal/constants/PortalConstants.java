package com.behappytwice.app.portal.constants;


public class PortalConstants {
	
	public static final String SIGNIN_USER_SESSION_KEY = "SIGNIN_USER";
	
	public static final String USER_MISMATCH_EXCEPTION_CODE = "U100";
	public static final String USER_MISMATCH_EXCEPTION_MESSAGE = "사용자 정보를 다시 확인하여 주세요.";
	
}///~