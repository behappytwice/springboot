package com.behappytwice.app.biz.base.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.behappytwice.app.biz.base.constants.enums.DataStatusEnum;
import com.behappytwice.app.biz.base.entity.RolePlayer;
import com.behappytwice.app.biz.base.repository.RolePlayerRepository;
import com.behappytwice.framework.base.component.BaseService;
import com.behappytwice.framework.util.IDGenerator;

@Service
public class RolePlayerService extends BaseService {

	@Autowired
	private RolePlayerRepository rolePlayerRepository;
	
	
	public void addRolePlayer(RolePlayer player) {
		
		Date createdTime = new Date(); 
		player.setRolePlayerID(IDGenerator.generateID()); 
		player.setCreatedTime(createdTime);
		player.setLastModifiedTime(createdTime);
		player.setDataStatus(DataStatusEnum.CREATED.getStatus());
		
		this.rolePlayerRepository.save(player);
		
		
	}//:
	
	
}///~
