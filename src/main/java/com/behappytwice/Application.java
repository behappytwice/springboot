package com.behappytwice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * SpringBoot application의 진입점이 되는 클래스이다.
 * 
 * 어플리케이션에 관련된 설정은 application.properties에 하고 기능구현에서 필요한 설정은 biz-config.properties 
 * 파일에 작성한다. 확장된 커스텀 기능을 구현할 때에는 biz-custom.properties 파일에 작성한다.
 * 
 * 
 * application.properties는 src/main/resources 폴더 아래에 두면 스프링이 자동으로 발견하여 읽기 때문에
 * PropertySource를 설정할 필요가 없다.  
 * 
 * 
 * @author behap
 */
@SpringBootApplication(scanBasePackages = { "com.behappytwice" }) 
@EnableAutoConfiguration(exclude = { DataSourceTransactionManagerAutoConfiguration.class, DataSourceAutoConfiguration.class })
//@ComponentScan(basePackages="com.behappytwice")
@PropertySources({ 
	@PropertySource(value = "classpath:biz-config.properties", ignoreResourceNotFound = true), 
	@PropertySource(value = "classpath:biz-custom.properties", ignoreResourceNotFound = true) 
})
public class Application   {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}/// ~
