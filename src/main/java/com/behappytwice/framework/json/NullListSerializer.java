package com.behappytwice.framework.json;


import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * java.util.List 클래스가 null인 경우를 처리를 위한 Serializer이다. POJO의 List<> 필드가 null인
 * 경우에 [] 을 값을 설정하기 위해서 사용한다. 
 * @author Sanghyun,Kim(sanghyun@naonsoft.com)
 *
 */
public class NullListSerializer  {
    
	/**
	 * Serializer 인스턴스 
	 */
	public static final JsonSerializer<Object> EMPTY_LIST_SERIALIZER_INSTANCE = new EmptyListSerializers();

    public NullListSerializer() {}

    private static class EmptyListSerializers extends JsonSerializer<Object> {
        public EmptyListSerializers() {}
        @Override
        public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
                throws IOException, JsonProcessingException {
            jsonGenerator.writeObject(new ArrayList());
        }
    }
}