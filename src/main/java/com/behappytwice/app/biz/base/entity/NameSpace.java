package com.behappytwice.app.biz.base.entity;


public class NameSpace {
	
	protected String nameSpaceID;
	protected String subject;

	
	public String getNameSpaceID() {
		return nameSpaceID;
	}

	public void setNameSpaceID(String nameSpaceID) {
		this.nameSpaceID = nameSpaceID;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
