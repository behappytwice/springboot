package com.behappytwice.framework.web.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.behappytwice.framework.util.json.JSONUtils;


//@InterceptorAnnotation(addPathPattern = { "/**" }, excludePathPattern = {})
//@AccessControlAllowOrigin(allowOrigin = { "*", "http://localhost" })
public class CorsInterceptor extends HandlerInterceptorAdapter {
	
	protected List<String> allowOrigin;
	
	public CorsInterceptor() {
		try {
//			Annotation[] annotations = this.getClass().getAnnotations();
//			for (Annotation annotation : annotations) {
//				if (annotation instanceof AccessControlAllowOrigin) {
//					AccessControlAllowOrigin allow = (AccessControlAllowOrigin) annotation;
//					this.allowOrigin = allow.allowOrigin();
//					break;
//				}
//			} // for
			
			this.allowOrigin = JSONUtils.toList("allow-origin.json", String.class);
			
		} catch (Exception e) {
			// Ignores this exception
			// TODO : display this error log
		}
	}//:
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		//System.out.println("★★★★★★★ CorsInterceptor");
		
		if(allowOrigin != null && allowOrigin.size() == 1 && allowOrigin.get(0).equals("*")) {
			response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
			response.setHeader("Access-Control-Max-Age", "3600");
			response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
			response.setHeader("Access-Control-Allow-Origin", "*");
		}else { 
			if(allowOrigin != null && allowOrigin.size() > 0) {
				for(String allowUrl : allowOrigin) {
					if( allowUrl.equals(request.getHeader("Origin")))  {
						response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
						response.setHeader("Access-Control-Max-Age", "3600");
						response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
						response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
						return true;
					}
				}
			}
		}
		return true;
	}//:
}///~
