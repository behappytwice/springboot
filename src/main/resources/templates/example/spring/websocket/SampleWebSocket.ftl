<html>
<meta charset="utf-8" />
<head>
<script src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js"></script>
<script type="text/javascript">

    // WebSocket 연결 URL 
	var wsUri = "ws://localhost/myHandler";
	var output; // 주고 받은 내용을 표시할 요소 참조 변수
	 
	/** 초기화 */
	function init() {
        var mybtn = document.getElementById("sendBtn");
        document.getElementById("sendBtn").addEventListener("click", onClickSendBtn);
		output = document.getElementById("output");
		testWebSocket();
	}

    /** WebSocket 생성 및 정의 */ 
	function testWebSocket() {
	    // WebSocket 생성
		websocket = new WebSocket(wsUri);
		// Socket 연결되었을 때 
		websocket.onopen = function(evt) {
			onOpen(evt);
		};
		// Socket이 종료되었을 때
		websocket.onclose = function(evt) {
			onClose(evt);
		};
		// 서버로부터 메시지를 받았을 때
		websocket.onmessage = function(evt) {
			onMessage(evt);
		};
		// 오류가 발생한 경우
		websocket.onerror = function(evt) {
			onError(evt);
		};
	}

    /** 연결 이벤트 핸들러 */
	function onOpen(evt) {
		writeToScreen('연결완료');
		doSend('TestMessage');
	}

    /** 종료 이벤트 핸들러 */
	function onClose(evt) {
		writeToScreen("Closed");
	}

    /** 메시지 수신 이벤트 핸들러 */
	function onMessage(evt) {
		writeToScreen("상대방:" + evt.data);
	}

    /** 메시지 전송 함수 */
	function doSend(message) {
		writeToScreen("나:" + message);
		websocket.send(message);
	}

    /** 화면에 내용을 쓴다. */
	function writeToScreen(message) {
		var pre = document.createElement("P");
		pre.style.wordWrap = "break-word";
		pre.innerHTML = message;
		output.appendChild(pre);
	}


    /** 버튼 클릭 이벤트  */
	function onClickSendBtn() {
		var txtbox = document.getElementById("sendMsg");
		var message = txtbox.value;
		doSend(message);
	}

	window.addEventListener("load", init, false);
	
</script>
</head>
<body>

	<h2>WebSocket Test</h2>
	<hr>
    <textarea id="sendMsg" rows="10" cols="60"></textarea><br>
	<input type="button" id="sendBtn" value="Send a message">
	<br>
	<div id="output" style="background-color:yellow;"></div>
</body>
</html>