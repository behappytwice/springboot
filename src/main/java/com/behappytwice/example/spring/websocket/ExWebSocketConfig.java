package com.behappytwice.example.spring.websocket;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;


/** WebSocket 핸들러 생성 및 등록을 위한 Java Configuration */ 
@Configuration
@EnableWebSocket
public class ExWebSocketConfig implements WebSocketConfigurer {

	/** 핸들러를 등록 한다 */
	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		//registry.addHandler(myHandler(), "/myHandler");  // WebSocket 사용시
		registry.addHandler(myHandler(), "/myHandler").withSockJS();  // SockJS 클라이언트 사용시
	}
	
	/** 핸들러를 생성한다. */
	@Bean
	public WebSocketHandler myHandler() {
		return new ExWebSocketHandler();
	}

}///~