package com.behappytwice.app.biz.organization.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.behappytwice.app.biz.base.constants.enums.RolePlayerTypeEnum;
import com.behappytwice.app.biz.base.repository.RolePlayerRepository;
import com.behappytwice.app.biz.organization.entity.Admin;
import com.behappytwice.app.biz.organization.entity.Member;
import com.behappytwice.app.biz.organization.repository.AdminRepository;
import com.behappytwice.app.biz.organization.repository.MemberRepository;
import com.behappytwice.framework.base.component.BaseService;
import com.behappytwice.framework.util.IDGenerator;


@Service
public class MemberService extends BaseService {

	@Autowired
	private MemberRepository memberRepository;
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private RolePlayerRepository rolePlayerRepopsitory;
	
	
	
	/**
	 * Find a member by the email address
	 * 
	 * @param email a user's email address
	 * @return
	 *    a Member instance
	 */
	public Member findByEmail(String email) { 
		return this.memberRepository.findByEmail(email);
	}
	
	/**
	 * Find a member by the name
	 * @param name  a user's name
	 * @return
	 * 	a member instance
	 *
	 */
	public Member findByName(String name) {
		return this.memberRepository.findByName(name);
	}
	
	
	@Transactional
	public void insertMember(Member member) {
		
		this.setTimeAndAccessorInfoForCreation(member);
		
		member.setRolePlayerID(IDGenerator.generateID());
		// this role player is a member
		member.setRolePlayerType(RolePlayerTypeEnum.MEMBER.getRolePlayerType());
		
		this.rolePlayerRepopsitory.save(member);
		logger.debug("■■■■ A role player inserted.");
		this.memberRepository.save(member);
		logger.debug("■■■■ A member inserted.");
		
	}//:
		
	@Transactional
	public void addAdmin(Admin admin) {
		this.setTimeAndAccessorInfoForCreation(admin);
		admin.setRolePlayerID(IDGenerator.generateID());
		// this role player is a member
		admin.setRolePlayerType(RolePlayerTypeEnum.ADMIN.getRolePlayerType());
		admin.setAdminType("1");
		this.rolePlayerRepopsitory.save(admin);
		this.adminRepository.save(admin);
	}//:
	
	
}///~
