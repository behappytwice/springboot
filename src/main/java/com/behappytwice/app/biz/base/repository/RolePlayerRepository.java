package com.behappytwice.app.biz.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.behappytwice.app.biz.base.entity.RolePlayer;

public interface RolePlayerRepository  extends JpaRepository<RolePlayer, String> {
	
}
