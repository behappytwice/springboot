package com.behappytwice.example.database.jpa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/** JPA를 테스트하기 위한 Bean이다. */
@Entity
@Table(name = "TEST_EMPLOYEE")
public class ExJpaEmployeeBean {

	@Id
	@Column(name = "user_id")
	private String userId;
	@Column(name = "name")
	private String name;
	@Column(name = "designation")
	private String designation;
	@Column(name = "salary")
	private int salary;
	@Column(name="enter_date")
	private Date enterDate;

	public ExJpaEmployeeBean() {
	}
	
	public ExJpaEmployeeBean(String userId, String name,  String designation, int salary) {
		this.userId = userId;
		this.name = name;
		this.designation = designation;
		this.salary = salary;
	}
	public ExJpaEmployeeBean(String userId, String name) {
		this.userId = userId;
		this.name = name;
	}
	
	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Date getEnterDate() {
		return enterDate;
	}
	public void setEnterDate(Date enterDate) {
		this.enterDate = enterDate;
	}
	
}
