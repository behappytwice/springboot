package com.behappytwice.framework.web.exception;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.behappytwice.framework.base.exception.BizException;
import com.behappytwice.framework.util.json.JSONUtils;
import com.behappytwice.framework.web.message.ResponseMessage;

/**
 * Controller에서 예외를 던지는 경우에 예뢰를 처리한다.
 * 
 * @author behap
 *
 */
@ControllerAdvice
@EnableWebMvc
public class GlobalExceptionHandler {
	
	private String DEFAULT_NOT_FOUND_VIEW = "error/error-404";

	
	/**
	 * DispatcherServlet가 처리할 핸들러를 찾지 못하는 경우에 예외를 잡는다. 그러나
	 * html, image 등 handler가 처리하지 않을 때에는 이 에러가 잡히지 않는다.
	 * (NOTE) 이 메소드가 호출되는 경우를 아직 보지 못했음. 필요 없으면 삭제할 것임.
	 * @param 
	 * @return
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	public String handle(Exception ex) {
		System.out.println(">>>>> NO HANDLER FOUND EXCEPTION");
		System.out.println(">>>>> NO HANDLER FOUND EXCEPTION");
		System.out.println(">>>>> NO HANDLER FOUND EXCEPTION");
		return DEFAULT_NOT_FOUND_VIEW;
	}// :
	
	
	/**
	 * Controller에서 예외가 발생하면 잡아서 처리한다.
	 * 
	 * @param request HttpServletRequest
	 * @param response  HttpServletResponse
	 * @param e  Exception 객체 
	 * @throws Exception
	 * 		예외 객체
	 */
	@ExceptionHandler(Exception.class)
	public void errorHandlerWithServiceRequest(HttpServletRequest request, HttpServletResponse response, Exception e)
			throws Exception {
		
		// Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3
		String accept = request.getHeader("Accept");
		if(accept == null) {
			throw e;
		}
		String[] accepts = accept.split(",");
		if(accepts.length > 1 || accepts.length == 0) {
			throw e; 
		}
		if( !(accepts[0].indexOf("application/json") >= 0 )) {
			throw e;
		}
		// Accept 값이 application/json이면 처리한다. 
		// handle json result 
		ResponseMessage resMessage = new ResponseMessage();
		if(e instanceof BizException) {
			BizException bizError = (BizException)e;
			resMessage.setStatus(bizError.getCode()); // set an error code to the status
			resMessage.setStatusText(bizError.getMessage()); // an error message
			String jsonMessage = JSONUtils.toJSON(resMessage); // convert the class, ResponseMessage to JSON string.
			response.setStatus(HttpStatus.OK.value());
			response.setCharacterEncoding("UTF-8");  // have to modify this
			response.setHeader("Content-Type", "application/json;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println(jsonMessage);
			out.close();
		}else {
			throw e;
		}
	}// :
}/// ~