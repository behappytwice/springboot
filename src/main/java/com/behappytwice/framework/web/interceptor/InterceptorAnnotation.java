package com.behappytwice.framework.web.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface InterceptorAnnotation {

	/**
	 * Set URL pattern string to process 
	 * @return
	 */
	public String[] addPathPattern();
	/**
	 * Set URL pattern string not to process
	 * @return
	 */
	public String[] excludePathPattern(); 
	
}////~
