package com.behappytwice.framework.json;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;


/**
 * LocalDateTime 타입의 필드를 ISO형식의 문자열로 변환하기 위해서 사용한다.
 * @author behap
 *
 */
public class LocalDateTimeSerializer extends StdSerializer<LocalDateTime> {

	private static String formatString = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatString);
	
	public LocalDateTimeSerializer() {
		this(null);
	}

	public LocalDateTimeSerializer(Class<LocalDateTime> t) {
		super(t);
	}

	@Override
	public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider arg2)
			throws IOException {
		//gen.writeString(formatter.format(value));
		gen.writeString(value.format(formatter));
		
	}//:

}/// ~
