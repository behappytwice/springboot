package com.behappytwice.framework.util;

import java.util.UUID;

/**
 * 아이디를 생성하는 메소드들을 제공한다. 
 * 
 * @author behap
 *
 */
public class IDGenerator {

	/**
	 * UUID를 생성합니다. 
	 * @return UUID 문자열
	 */
	public static String generateID() {
		return UUID.randomUUID().toString().replace("-", "");
	}//
}
