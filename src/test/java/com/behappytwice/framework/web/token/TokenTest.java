package com.behappytwice.framework.web.token;

import java.security.Key;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

import org.junit.Test;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

/** JWT Test Case */
public class TokenTest {
	
	/** Base64 encode, decode 테스트케이스이다. */
	@Test
	public void testBase64() {
		String testString = "한글입니다.ABC";
		String encodedStr = Base64.getEncoder().encodeToString(testString.getBytes());
		System.out.println(encodedStr);
		byte[] decodedBytes = Base64.getDecoder().decode(encodedStr.getBytes());
		String decodedString = new String(decodedBytes);
		System.out.println(decodedString);
	}//:
	
	
	/** Token 쓰기, 읽기 테스트 케이스이다. */
	@Test 
	public void testToken() throws Exception {
		// JWS 쓰기 
		// 알고리즘 선택 
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		// 키 생성 
		Key key = Keys.secretKeyFor(signatureAlgorithm);
		// 키 바이트로 변환 
		byte[] encodedKey = key.getEncoded();
		// base64로 변환 
		String encodedString = Base64.getEncoder().encodeToString(encodedKey);
		// base64를 byte[]로 변환 
		byte[] decodedByte   = Base64.getDecoder().decode(encodedString.getBytes());
		// byte[]로 Key 생성 
		Key key2 = Keys.hmacShaKeyFor(decodedByte);
		
		// 현재시간 
		Date now = new Date();
		// 현재 날짜 시간의 timestamp 
		long nowmills = now.getTime();
		// 유효기간 : 하루 
		long ttlMillis = 60 * 60 * 24;
		// 만료 날짜,시간 설정을 위한 timestamp 
		long expiration = nowmills + ttlMillis;
		// 만료 날짜,시간 Date 생성 
		Date expDate = new Date(expiration);
		// jws생성 
		String jws = Jwts.builder()
			    .setIssuer("me")
			    .setSubject("Bob")
			    .setAudience("you")
			    .claim("user", "Andy")
			    .claim("password", "123456")
			    .setExpiration(expDate) //a java.util.Date
			    .setId(UUID.randomUUID().toString())//just an example id
			    .signWith(key2)
			    .compact();
		System.out.println(jws);
		
		// JWS 읽기 
		Jws<Claims> jws2  = Jwts.parser().setSigningKey(key2).parseClaimsJws(jws);
		jws2.getBody().getSubject();
		jws2.getBody().getIssuer();
		jws2.getBody().getAudience();
		jws2.getBody().getId();
		jws2.getBody().getExpiration();
		// key를 사용하여 claim 값을 구함 
		String user  = (String)jws2.getBody().get("user");
		System.out.println(user);
		String password  = (String)jws2.getBody().get("password");
		System.out.println(password);
		// 만료기간 구함 
		Date rexpdate = jws2.getBody().getExpiration();
		System.out.println("Expiration:" + rexpdate);
		
		// 현재시간 
		Date now2 = new Date();
		long now2Mills = now2.getTime();
		long rexpMills = rexpdate.getTime();
		// 토큰이 만료되지 않았는지 체크 
		if(rexpMills >= now2Mills) {
			System.out.println("유효한 토큰");
		}else { 
			System.out.println("유효하지 않은 토큰");
		}
	}//:
}///~