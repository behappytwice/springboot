package com.behappytwice.app.biz.organization.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.behappytwice.app.biz.base.entity.RolePlayer;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="admin")
@Getter
@Setter
public class Admin extends RolePlayer {
	
	@Column(name="admin_type")
	private String adminType;
	

}
