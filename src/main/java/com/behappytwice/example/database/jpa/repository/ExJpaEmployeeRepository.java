package com.behappytwice.example.database.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.behappytwice.example.database.jpa.entity.ExJpaEmployeeBean;

/**
 * EntityManager를 쓰기 위해 EntityManager를 사용할 JpaEmployeeRepositoryCustom 인터페이스를 정의하고 
 * 그 인터페이스를 구현하는 구현체를 작성한다. 이 Repository interface에서 JpaRepository 인터페이스와 
 * JpaEmployeeRepositoryCustom 인터페이스를 상속받는다.
 * 
 * @author behap
 *
 */
public interface ExJpaEmployeeRepository extends JpaRepository<ExJpaEmployeeBean, String>, ExJpaEmployeeRepositoryCustom {
	
	/** userId로 찾기 */
	ExJpaEmployeeBean findByUserId(String id);
	/** 이름으로 찾기 */
	ExJpaEmployeeBean findByName(String name);
	/** 이름과 직위로 찾기 */
	ExJpaEmployeeBean findByNameAndDesignation(String name, String designation);
	/** 이름 또는 직위로 찾기 */
	List<ExJpaEmployeeBean> findByNameOrDesignation(String name, String designation);
	/** 급여가  less than(<) 인 직원 찾기 */
	List<ExJpaEmployeeBean> findBySalaryLessThan(int salary);
	
	@Query(value="select count(*) from employee", nativeQuery=true)
	List<Long> selectUserCount();
		
	@Query(value="select * from employee ", nativeQuery=true)
	List<ExJpaEmployeeBean> findAllEmployees();
	
	@Query(value="select * from employee where user_id = :userid ", nativeQuery=true)
	ExJpaEmployeeBean findEmployeeById(@Param("userid") String userId);
	
	@Query(value="select user_id, name from employee where user_id = :userid ", nativeQuery=true)
	ExJpaEmployeeBean findEmployeeById2(@Param("userid") String userId);
	
	
}///:
