<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<#include "/framework/include/BootStrapCss.ftl">
	<style>
	</style>
</head>
<body>
	<div id="app"></div>
	<script type="module" src="/app/portal/Portal.js"></script>
	<#include "/framework/include/CommonJavaScript.ftl">
</body>
</html>