package com.behappytwice.app.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.behappytwice.app.portal.service.MainService;
import com.behappytwice.framework.base.component.BaseController;
import com.behappytwice.framework.web.message.ResponseMessage;

@Controller
@RequestMapping(value="/main")
public class MainController extends BaseController {
	
	@Autowired
	private MainService mainService; 
	
	@RequestMapping(value="/main")
	public String main(HttpServletRequest request) {
		// Checks if a session exists
		return "app/portal/Main";
	}//:
	
	@RequestMapping(value="/signout")
	public @ResponseBody ResponseMessage  signOut(HttpServletRequest request) {
		this.mainService.signOut();
		return this.createSuccessMessage(null);
	}//:

}///~
