package com.behappytwice.framework.json;


import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * java.lang.String 클래스가 null인 경우를 처리하기 위한 Serializer이다. POJO의 String 타입 필드가
 * null이면 "" 을 설정하기 위해서 사용한다.
 * 
 * @author Sanghyun,Kim(sanghyun@naonsoft.com)
 *
 */
public class NullStringSerializer  {
	/**
	 * Searialzer 인스턴스 
	 */
    public static final JsonSerializer<Object> EMPTY_STRING_SERIALIZER_INSTANCE = new EmptyStringSerializer();

    /**
     * Constructor 
     */
    public NullStringSerializer() {}


    private static class EmptyStringSerializer extends JsonSerializer<Object> {
        public EmptyStringSerializer() {}

        @Override
        public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
                throws IOException, JsonProcessingException {
            jsonGenerator.writeString("");
        }
    }
}