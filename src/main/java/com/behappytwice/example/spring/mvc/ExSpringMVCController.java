package com.behappytwice.example.spring.mvc;

import java.net.URLDecoder;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.behappytwice.framework.base.component.BaseController;
import com.behappytwice.framework.util.xml.XmlUtils;
import com.behappytwice.framework.web.message.ResponseMessage;
import com.behappytwice.example.model.ExUserBean;

/**
 * Spring Controller를 사용하는 방법을 설명하는 클래스이다. 
 * @author behap
 *
 */
@Controller
@RequestMapping(value="/example/springmvc")
public class ExSpringMVCController extends BaseController {
	
	@RequestMapping(value="/main")
	public String main() {
		return "sample/springmvc/SpringMVC";
	}//

	/** request.getParameter() 사용하는 예이다. */
	@RequestMapping(value="/reqParamBasic")
	public @ResponseBody ResponseMessage reqParam(HttpServletRequest request, HttpServletResponse response) {
		System.out.println(request.getParameter("userName"));
		return this.createSuccessMessage(null);
	}//:
	
	
	/** queryString 파라미터를 name 변수와 바인딩한다. */
	@RequestMapping(value="/reqParam")
	public @ResponseBody ResponseMessage  requestParamTest(@RequestParam(name="name") String name) {
		System.out.println("Request Param:" + name);
		return this.createSuccessMessage(null);
	}//:
	
	
	/** JSON을 객체와 바인딩한다. */
	@RequestMapping(value="/postJSON")
	public @ResponseBody ResponseMessage  formPost(HttpServletRequest request, HttpServletResponse response, @RequestBody ExUserBean user) {
		System.out.println("User Name:" + user.getUserName());
		System.out.println("Entered Date:" + user.getEnteredDate());
		return this.createSuccessMessage(null);
	}//:
	
	/** Form Data를 객체와 바인딩 한다.*/
	@RequestMapping(value="/postUrlencoded", consumes="application/x-www-form-urlencoded")
	public @ResponseBody ResponseMessage postUrlencoded(HttpServletRequest request, HttpServletResponse response, @ModelAttribute  ExUserBean user) {
		System.out.println("User Name:" + user.getUserName());
		return this.createSuccessMessage(null);
	}//:

	
	/** XML을 String과 바인딩 한다. */
	@RequestMapping(value="/postXml", consumes = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody ResponseMessage postXml(HttpServletRequest request, HttpServletResponse response, @RequestBody String xml) {
		System.out.println(xml);
		return this.createSuccessMessage(null);
	}//:
	
	
	/** text를 String과 바인딩한다.*/
	@RequestMapping(value="/postText", consumes = MediaType.TEXT_PLAIN_VALUE)
	public  @ResponseBody ResponseMessage  postText(HttpServletRequest request, HttpServletResponse response, @RequestBody String text) throws Exception {
		System.out.println(URLDecoder.decode(text, "utf-8"));
		return this.createSuccessMessage(null);
	}//:
	
	
	
	/** JSON을 반환한다. */
	@RequestMapping(value="/jsonTest", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ExUserBean jsonTest() throws Exception {
		ExUserBean bean = new ExUserBean();
		bean.setUserId("abced");
		bean.setUserName("HelloKim");
		bean.setEnteredDate(new Date());
		return bean;
	}//:
	
	/** XML을 반환한다. */
	@RequestMapping(value="/xmlTest", produces="text/xml;charset=utf-8") 
	public @ResponseBody String  xmlTest() throws Exception {

		ExUserBean bean = new ExUserBean();
		bean.setUserId("abced");
		bean.setUserName("HelloKim");
		
		return XmlUtils.marshal(ExUserBean.class, bean);
	}//:

	/** HTML을 반환한다 */
	@RequestMapping(value="/htmlTest", produces="text/html;charset=utf-8") 
	public @ResponseBody String  htmlTest() throws Exception {
		String html = "<html><body><h2>Hello World</h2></body></html>";
		return html;
	}//:

	/** text를 반환한다. */
	@RequestMapping(value="/textTest", produces="text/plain;charset=utf-8") 
	public @ResponseBody String  textTest() throws Exception {
		String text = "This is a plain text.";
		return text;
	}//:
	
	
}///~
