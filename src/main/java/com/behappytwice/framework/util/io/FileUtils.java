package com.behappytwice.framework.util.io;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 파일을 읽고 쓰기 위한 유틸리티 클래스이다. 
 * @author behap
 *
 */
public class FileUtils {
	
	/**
	 * byte[] 배열을 파일에 쓴다. 
	 * @param fileName  파일 경로 
	 * @param data  파일에 쓸 데이터 
	 * @throws IOException
	 */
	public void writeWithStream(String fileName, byte[] data) throws IOException {
		File f = new File(fileName);
		writeWithStream(f, data);
	}//:

	/**
	 * byte[] 배열을 파일에 쓴다.
	 * @param f   쓸 파일 객체 
	 * @param data 파일에 쓸 데이터 
	 * @throws IOException
	 */
	public void writeWithStream(File f, byte[] data) throws IOException {
		FileOutputStream outputStream = new FileOutputStream(f);
		outputStream.write(data);
		outputStream.close();
	}//:
	
	/**
	 * 문자열을 파일로 쓴다. 
	 * @param fileName  파일 경로 
	 * @param data  파일에 쓸 데이터 
	 * @throws IOException
	 */
	public void write(String fileName, String data) throws IOException {
		File f = new File(fileName);
		write(f, data);
	}//:
	

	/**
	 * 문자열을 파일로 쓴다.
	 * @param f  파일 객체 
	 * @param data  파일에 쓸 데이터 
	 * @throws IOException
	 */
	public void write(File f, String data) throws IOException {
		FileOutputStream fos = new FileOutputStream(f);
		DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));
		outStream.writeUTF(data);
		outStream.close();
	}//:

}// ~
