package com.behappytwice.example.spring.websocket.stomp;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver;
import org.springframework.messaging.handler.invocation.HandlerMethodReturnValueHandler;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

/** Stomp를 사용하기 위한 WebSocket 설정한다. */
@Configuration
@EnableWebSocketMessageBroker
public class ExStompWebSocketConfig implements WebSocketMessageBrokerConfigurer {

	/** 메시지 브로커를 설정한다. */
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		// 메시지 브로커가 /topic이 들어가는 구독자들에게 메시지를 전달한다 
		registry.enableSimpleBroker("/topic");
		// 클라이언트가 서버에게 /app를 붙이고 메시지를 전달할 주소 
		registry.setApplicationDestinationPrefixes("/app");
	}
	/** EndPoint를 등록한다. */ 
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		// 클라이언트가 서버에 접속할 Endpoint를 설정한다. 엔드포인트는 여러개 추가 가능하다
		// client에서 websocket 대신 향상된 SockJS롤 접속하려면 .withSockJS()를 붙여준다. 
		registry.addEndpoint("/stompWebSocketHandler").withSockJS();
	}
	/** MessageConerter를 설정한다. */
	@Override
	public boolean configureMessageConverters(List<MessageConverter> messageConverters) {
		// true를 반환하지 않으면 오류 발생, 디폴트 값은 false 
		return true;
	}

}///~