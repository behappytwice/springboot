package com.behappytwice.framework.log;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** 
 * Logger를 삽입하기 위한 어노테이션이다. 
 * @author behap
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
@Documented
public @interface Log {

}
