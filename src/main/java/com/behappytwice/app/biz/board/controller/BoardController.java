package com.behappytwice.app.biz.board.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/board/board")
public class BoardController {

	@RequestMapping(value="/main")
	public String main(HttpServletRequest request) {
		return "biz/board/BoardMain";
	}
	
}///~
