package com.behappytwice.framework.json;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class DateSerializer extends StdSerializer<Date> {

	private static String formatString = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private SimpleDateFormat formatter =   new SimpleDateFormat(formatString);
	
	public DateSerializer() {
		this(null);
	}

	public DateSerializer(Class<Date> t) {
		super(t);
	}

	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeString(formatter.format(value));
	}

}/// ~