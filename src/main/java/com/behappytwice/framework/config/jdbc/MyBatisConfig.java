package com.behappytwice.framework.config.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.behappytwice.framework.constants.FrameworkConstants;
import com.behappytwice.framework.util.json.JSONUtils;

/**
 * Spring에서 MyBatis를 사용하기 위한 Configuration class이다. Mybatis 버전 2.0.2 버전 문서를
 * 참고했다. Spring에서 MyBatis를 사용하기 위해서는 SqlSessionFactory와 Mapper interface가 필요하다.
 * SqlSessionFactoryBean은 SqlSessionFactory를 생성하기 위해 필요하다.
 * SqlSessionFactoryBean은 DataSource를 필요로 한다. Mapper interface는
 * MapperFactoryBean을 사용하여 Spring에 추가된다. Mapper class는 interface여야 한다.
 * 
 * Mybatis와 관련된 설정 속성
 * 
 * # application.properties
 * mybatis.type-aliases-package=com.example.domain.model
 * mybatis.type-handlers-package=com.example.typehandler
 * mybatis.configuration.map-underscore-to-camel-case=true
 * mybatis.configuration.default-fetch-size=100
 * mybatis.configuration.default-statement-timeout=30
 * 
 * @author behap
 * @see https://mybatis.org/spring/getting-started.html
 */
@Configuration
@MapperScan(basePackages = { "com.behappytwice" })
public class MyBatisConfig {

	@Autowired
	private Environment env;

	/**
	 * SqlSessionFactory를 생성한다.
	 * 
	 * @param dataSource
	 * @param applicationContext
	 * @return
	 * @throws Exception
	 */
	@Primary
	@Bean(name = "sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactoryBean(@Autowired @Qualifier("dataSource") DataSource dataSource,
			ApplicationContext applicationContext) throws Exception {

		System.out.println("●●●●●●SqlSessionFactoryBean:" + env.getProperty("db.type"));

		// https://mybatis.org/spring/getting-started.html
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(dataSource);

		String dbType         = env.getProperty(FrameworkConstants.DB_TYPE);
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		
		// * zeror or more 문자들 
		// ? 하나의 문자와 매치 
		// ** 디렉토리 이름으로 사용되면 zero 또는 하나 이상의 디렉토리와 매칭된다.
		// /CVS/* CVS 디렉토리 아래의 모든 파일애 먜칭된다. 
		// 
		String mapperLocation = "classpath*:com/behappytwice/app/sqlmap/" + dbType + "/**/*.xml";
		
		org.springframework.core.io.Resource[] mapperResource = null;
		List<org.springframework.core.io.Resource> mapperList = new ArrayList<org.springframework.core.io.Resource>();
		mapperResource = resolver.getResources(mapperLocation);
		for(org.springframework.core.io.Resource res : mapperResource) {
			mapperList.add(res); 
		}

		
		try { 
			List<String> list = JSONUtils.toList("mapper-custom.json", String.class);
			for(String extPath : list) {
				String locPath = "classpath*:" + extPath.trim() + "/*Mapper.xml";
				org.springframework.core.io.Resource[] mapres = resolver.getResources(extPath.trim());
				for(org.springframework.core.io.Resource res : mapres) {
					mapperList.add(res); 
				}
			}
		}catch(Exception e) {
			//ignores
		}
		org.springframework.core.io.Resource[] resourceToReturn = new org.springframework.core.io.Resource[mapperList.size()];
		resourceToReturn = mapperList.toArray(resourceToReturn);
		factoryBean.setMapperLocations(resourceToReturn);
		return factoryBean.getObject();
	}// :
	

}/// ~
