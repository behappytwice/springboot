<html>
<meta charset="utf-8" />
<head>
<script	src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js"></script>
<script type="text/javascript">

    // WebSocket URL. ws://는 사용하지 않고 일반적인 http:// 프로토콜을 사용한다.
	var wsUri = "http://localhost/myHandler";
	var output;
	var sock = null;

    /** 초기화 */ 
	function init() {
		var mybtn = document.getElementById("sendBtn");
		document.getElementById("sendBtn").addEventListener("click", onClickButton);
		output = document.getElementById("output");
		testWebSocket();
	}
	
	/** SockJS 생성 및 핸들러 정의 */
	function testWebSocket() {
	
	    /** SockJS 생성 */ 
		sock = new SockJS(wsUri);
		
		/** Connection 연결 */ 
		sock.onopen = function() {
			log('Connection opened');
			send('TestMessage');
		};
		
		/** 메시지 수신 */
		sock.onmessage = function(e) {
			log("Message received:"  + e.data);
		};

		/** 커넥션 종료  */ 
		sock.onclose = function() {
			console.log('close');
			log("Connection closed.");
		};
		
	}// testWebSocket


    /** 웹소켓 연결 종료 시킨다 */
	function close() {
		sock.close();
	}


    /** 서버로 메시지 전송 */
	function send(message) {
		log("Message to send:" +  message);
		sock.send(message);
	}
	
	/** 버튼 클릭 이벤트 핸들러 */
    function onClickButton() {
        var txtbox = document.getElementById("sendMsg");
        var message = txtbox.value;
        send(message);
    }
    

    /** 화면에 메시지 출력 */    
	function log(message) {
		var pre = document.createElement("P");
		pre.style.wordWrap = "break-word";
		pre.innerHTML = message;
		output.appendChild(pre);
	}

    // 초기화 이벤트 설정 
	window.addEventListener("load", init, false);
	
</script>
</head>
<body>
	<h2>SockJS를 이용한 WebSocket Test</h2>
	메시지 <input type="text" id="sendMsg" value="">&nbsp;
	<input type="button" id="sendBtn" value="Send a message">
	<br>
	<div id="output"></div>
</body>
</html>