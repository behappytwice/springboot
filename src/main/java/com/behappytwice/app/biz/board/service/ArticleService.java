package com.behappytwice.app.biz.board.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.behappytwice.app.biz.base.constants.enums.DataStatusEnum;
import com.behappytwice.app.biz.base.constants.enums.EntryTypeEnum;
import com.behappytwice.app.biz.base.repository.EntryRepository;
import com.behappytwice.app.biz.board.entity.Article;
import com.behappytwice.app.biz.board.repository.ArticleRepository;
import com.behappytwice.framework.base.component.BaseService;
import com.behappytwice.framework.util.IDGenerator;

@Service
public class ArticleService extends BaseService {

	
	@Autowired
	private ArticleRepository articleRepository;
	@Autowired
	private EntryRepository entryRepository;
	
	@Transactional
	public void addArticle(Article article) {
		// if the article is null, an Exception will be thrown
		//this.assertNullThrowException(article);
		this.setTimeAndAccessorInfoForCreation(article);
		article.setEntryId(IDGenerator.generateID());
		article.setReplyCount(0);
		article.setEntryType(EntryTypeEnum.BOARD.getEntryrType());
		this.entryRepository.save(article);
		this.articleRepository.save(article);
	}//:
	
	
	
	@Transactional
	public void modifyArticle(Article article) {
		Date today = new Date();
		article.setLastModifiedTime(today);
		article.setDataStatus(DataStatusEnum.MODIFIED.getStatus());
		this.entryRepository.updateSubject(article.getSubject(), article.getLastModifiedTime(), article.getEntryId());
	}//:
	
	public Article selectArticle(String entryId) {
		return this.articleRepository.findByEntryId(entryId);
	}
	
	/**
	 * 모든 게시글을 조회한다.
	 * 
	 * @return
	 * 		List<Article>
	 */
	public List<Article> selectAllArticles() {
		return this.articleRepository.findAll();
	}
	
}///~
