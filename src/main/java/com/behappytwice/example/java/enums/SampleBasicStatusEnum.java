package com.behappytwice.example.java.enums;

/** 가장 기초적인  Enum */
public enum SampleBasicStatusEnum {
	PENDING,
	ACTIVE,
	INACTIVE,
	DELETED
}
