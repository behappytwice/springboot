package com.behappytwice.framework.crypt;

import org.junit.Test;

/**  
 * CryptUtils 클래스 테스트 케이스이다. 
 * @author behap
 *
 */
public class CryptUtilsTest {

	/** Digest 테스트 케이스*/
	@Test
	public void testDigest() throws Exception {
		String str = "한국이다.";
		// MD5 테스트
		String hex = CryptUtils.getDigest().digestToHex(str, AlgorithmEnum.MD5);
		System.out.println(hex);
		// SHA-256 테스트
		String hex2 = CryptUtils.getDigest().digestToHex(str, AlgorithmEnum.SHA256);
		System.out.println(hex2);
		// SHA3-256 테스트
		String hex3 = CryptUtils.getDigest().digestToHex(str, AlgorithmEnum.SHA3256);
		System.out.println(hex2);
	}//:
	
	/** AES 테스트 케이스 */
	@Test 
	public void testAES() throws Exception  {
		String key = "0123456789123456";
		String str = "한국이다.";
		String base64 = CryptUtils.getAES().encrypt(key, str);
		System.out.println(base64);
		String strDecrypted = CryptUtils.getAES().decrypt(key, base64);
		System.out.println(strDecrypted);
		
	}//:
	
	
}///~
