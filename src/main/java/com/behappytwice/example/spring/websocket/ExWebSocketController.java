package com.behappytwice.example.spring.websocket;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.behappytwice.framework.base.component.BaseController;


/** WebSocket을 테스트하기 위한 Controller이다. */
@Controller
@RequestMapping(value="/example/spring/websocket")
public class ExWebSocketController  extends BaseController {

	/** WebSocket 테스트 화면 */
	@RequestMapping(value="/websocket")
	public String websocket() {
		return "sample/spring/websocket/SampleWebSocket";
	}//
	/** SockJS를 이용한 WebSocket 테스트 화면 */
	@RequestMapping(value="/sockjs")
	public String sockjs() {
		return "sample/spring/websocket/SampleWebSocket-SockJs";
	}//
	
}///~
