package com.behappytwice.framework.crypt;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.junit.Test;

public class CryptTest {

	
	private static String bytesToHex(byte[] hash) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}
		return hexString.toString();
	}
	
	

	/**
	 * SHA256, SHA3-256 테스트케이스이다.
	 * @throws Exception
	 */
	@Test
	public void testSHA3_256() throws Exception {
		// JDK 9 이상
		String originalString = "Hello world";
		// getInstance("SHA-256")으로 사용 가능 
		final MessageDigest digest = MessageDigest.getInstance("SHA3-256");
		final byte[] hashbytes = digest.digest(originalString.getBytes(StandardCharsets.UTF_8));
		String sha3_256hex = bytesToHex(hashbytes);
		System.out.println(sha3_256hex);
	}// :

	@Test
	public void testMD5() throws NoSuchAlgorithmException {
		String hash = "35454B055CC325EA1AF2126E27707052";
		String password = "ILoveJava";

		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(password.getBytes());
		byte[] digest = md.digest();
		String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
		System.out.println(myHash);
		if (myHash.equals(hash)) {
			System.out.println("True");
		}
	}// :

	
	
	@Test
	public void testAES() throws Exception {
		String strToEncrypt = "한글입니다.";
		// 16 bytes 이어야 함
		String key = "1234567890123456";
		SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		String encryptedStr = Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		System.out.println(encryptedStr);

		Cipher cipher2 = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher2.init(Cipher.DECRYPT_MODE, secretKey);
		String decreptedStr = new String(cipher2.doFinal(Base64.getDecoder().decode(encryptedStr)));
		System.out.println(decreptedStr);

	}// :


	/**
	 * RSA 테스트케이스이다. 
	 * @throws Exception
	 */
	@Test 
	public void testRSA() throws Exception {
		String originalStr = "한글입니다.";
		KeyPair keyPair = RSAUtils.createKeyPair();
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();
		byte[] byteEncrypted  = RSAUtils.encrypt(originalStr.getBytes(), publicKey);
		byte[] byteDecrypted = RSAUtils.decrypt(byteEncrypted, privateKey);
		String strDecrypted = new String(byteDecrypted);
		System.out.println(strDecrypted);
	}//:
	
	
}/// ~
