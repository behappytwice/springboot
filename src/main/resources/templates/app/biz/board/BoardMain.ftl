<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<#include "/framework/include/BootStrapCss.ftl">
<style>

/* div {
  border-style:solid;
  border-width:1px;
}
 */
 
 .mouse-pointer {
    cursor : pointer;
 }
 
 
</style>
<!-- <link rel="stylesheet" type="text/css" href="/sample/vuejs/css/main.css"> -->
</head>
<body>

<div id="app"> 
</div>


<script type="module" src="/biz/js/board/BoardMain.js"></script> 

<#include "/framework/include/CommonJavaScript.ftl">

</body>
</html>