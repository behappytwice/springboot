package com.behappytwice.framework.web.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.behappytwice.framework.constants.FrameworkConstants;
import com.behappytwice.framework.web.session.SessionThreadLocal;


public class ThreadLocalInterceptor implements HandlerInterceptor  {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		//System.out.println("★★★★★★★ ThreadLocalInterceptor");
		
		Map map = new HashMap();
		map.put(FrameworkConstants.THREAD_LOCAL_HTTP_REQUEST_KEY, request);
		map.put(FrameworkConstants.THREAD_LOCAL_HTTP_RESPONSE_KEY, response);
		SessionThreadLocal.setValue(map);
		return true; 
	}//:


	
}
