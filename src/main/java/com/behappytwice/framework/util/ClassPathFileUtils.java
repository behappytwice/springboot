package com.behappytwice.framework.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

/** 
 * 클래스 패스에 있는 파일을 다룬다. 
 * @author behap
 *
 */
public class ClassPathFileUtils {

	
	/**
	 * 클래스 패스에 있는 파일을 읽어서 문자열로 반환한다.  
	 * 
	 * @param path 
	 *      클래스패스의 파일경로 - "com/sanghyun/aaa.txt" 와 같이 경로를 설정
	 * @return
	 * @throws Exception
	 */
	public static String readFile(String path) throws Exception {
		ClassPathResource resource = new ClassPathResource(path);
		FileReader fr = new FileReader(resource.getFile());
		String line = null;
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(fr);
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		br.close();
		return sb.toString();
	}// :

	/**
	 * 클래스패스에 있는 파일을 읽어서 List<String>으로 반환한다. 
	 * @param path 
	 *      클래스패스의 파일경로 - "com/sanghyun/aaa.txt" 와 같이 경로를 설정
	 * @return
	 * @throws Exception
	 */
	public static List<String> readFileToList(String path) throws Exception {
		
		List<String> list = new ArrayList<String>();
		
		ClassPathResource resource = new ClassPathResource(path);
		FileReader fr = new FileReader(resource.getFile());
		String line = null;
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(fr);
		while ((line = br.readLine()) != null) {
			//sb.append(line);
			list.add(line);
		}
		br.close();
		return list; 
	}// :
	
	
	
	/**
	 * 클래스패스에 있는 파일을 File객체로 반환한다. 
	 * @param path 
	 *     클래스패스의 파일경로 - "com/sanghyun/aaa.txt" 와 같이 경로를 설정
	 * @return
	 * @throws IOException
	 */
	public static File getFileObject(String path)  throws IOException  {
		ClassPathResource resource = new ClassPathResource(path);
		return resource.getFile();
	}// :
	
}///~
