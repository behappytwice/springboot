package com.behappytwice.example.database.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.behappytwice.framework.base.component.BaseController;

@Controller
@RequestMapping(value="/example/mybatis")
public class ExMybatisController extends BaseController {

	
	@Autowired
	private ExMybatisService mybatisTestService;
	
	@RequestMapping(value="/test")
	public String test() throws Exception {
		return "OK";
	}

	
	@RequestMapping(value="/insert")
	public String insert() throws Exception {

		this.mybatisTestService.insert();
		logger.debug("A data inserted.");
		return "index";
	}

}
