import Ajax     from '/framework/js/Ajax.js';
import Utils    from '/framework/js/Utils.js';
import EventBus from '/framework/js/EventBus.js';

export default {
  template: `
     <div>
        <b-form>
              <h3>Register</h3>
              <hr>
              <label for="up-member-name">User Name</label>
		      <b-input v-model="member.name" :state="nameValidation" id="up-member-name"  required></b-input>
		      <b-form-invalid-feedback :state="nameValidation">
		         Your password must be 3 characters long.
		      </b-form-invalid-feedback>
              
              <label for="up-member-email">Email</label>
		      <b-input type="email"    v-model="member.email" :state="emailValidation" id="up-member-email"   required></b-input>
		      <b-form-invalid-feedback :state="emailValidation">
		        {{control.desc.email}}
		      </b-form-invalid-feedback>
              
              <label for="up-member-email2">Email 2</label>
		      <b-input type="email"  v-model="member.email2" :state="emailValidation" id="up-member-email2"  @focus="onFocusEmail2"  required></b-input>
		      <b-form-invalid-feedback :state="emailValidation">
		        {{control.desc.email}}
		      </b-form-invalid-feedback>

              <label for="up-member-password">Password</label>
		      <b-input type="password" v-model="member.password"  :state="passwordValidation" id="up-member-password"></b-input>
		      <b-form-invalid-feedback :state="passwordValidation">
		         Your password must be 4 - 15 characters long.
		      </b-form-invalid-feedback>

		     </b-form>
		     <br>
		     
		     <b-button :disabled="control.disabled.button" variant="success" @click="onButtonClick">Submit</b-button>
        </b-form>
        <b-modal id="modal-1" title="Registration" ok-only>
              <p class="my-4">You've been registered.</p>
        </b-modal>
        
     </div>
  `,
    // data section 
  data() {
    return {
    	message : {
    	},
    	control : {
    		
    		desc : {
    			name   : "",
    			email :  ""
    		},
    		// 화면요소를 enable하거나 disabled하기 위한 변수들 
    		disabled  : {
    			// button
    			button : true
    		},
    		// 화면의 상태를 관리하기 위한 변수들
    		status : { 
    		}
    	},
    	// Entity
    	member : {
    		name: "",
    		email:"",
    		email2: "",
    		password:""
    	}
    }
  },
  // constructor section 
  created() {
  },
  mounted() {
  },
  computed : {
	  nameValidation() {
		  this.validateButton();
		  return this.validateName();
	  },
	  emailValidation() {
		  this.validateButton();
		  return this.validateEmail();
	  },
	  passwordValidation() {
		  this.validateButton();
		  return this.validatePassword();
	  }
  },
  // method section 
  methods: {
	  
	  // ============================================================================ event section 
	  onFocusEmail2 : function() {
		  if(Utils.isEmpty(this.member.email)) {
			  return; 
		  }
		  this.findByEmail();
	  },
	  onButtonClick: function() {
		this.insertMember();  
	  },
	  // ============================================================================ control section
	  validateEmail : function(isSubmit) {
  
		  if(Utils.isEmpty(this.member.email)) {
			  if(isSubmit) return false;
			  return;
		  }
		  if(this.member.email != this.member.email2) {
			  this.control.desc.email = "Your email doesn't match.";
			  return false;
		  }
		  if(Utils.validateEmail(this.member.email) ) {
			  //this.control.desc.email = "";
			  return true;
		  } else {
			  this.control.desc.email = "Your email format is wrong.";
			  return false;
		  }
		  return true;

	  },
	  validatePassword : function(isSubmit) {
		  if(Utils.isEmpty(this.member.password)) {
			  if(isSubmit) return false;
			  return;
		  }
		  if(this.member.password.length < 4 || this.member.password.length > 15) {
			  return false;
		  }
		  return true;
		  
	  },
	  validateName : function(isSubmit) {
		  if(Utils.isEmpty(this.member.name)) {
			  if(isSubmit) return false;
			  return
		  }
		  return this.member.name.length >= 3;
	  },
	  
	  validateButton : function() {

		  if(!this.validateEmail(true)){
			  this.control.disabled.button = true;
			  return false;
		  }
		  if(!this.validateName(true)){
			  this.control.disabled.button = true;
			  return false;
		  }

		  if(!this.validatePassword(true)){
			  this.control.disabled.button = true;
			  return false;
		  }
		  this.control.disabled.button = false;
		  return true;
		  
	  },
	  
	  // ============================================================================ transaction section 
	  /**
	   * ----------------------------------------------------------------------------------------
	   * 멤버를 추가한다
	   * ---------------------------------------------------------------------------------------- 
	   */
	  insertMember : function() {
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  data : this.member,
				  url : '/portal/insertMember',
				  success : function(response) {
					  console.log("success");
					  console.log(response);
					  if(response.data.status == "200") {
						  console.log("A member inserted.");
						  self.control.disabled.button = true;
						  self.$bvModal.show("modal-1");
					  }
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  },
	  /** 
	   * ---------------------------------------------------------------------------------------- 
	   * email이 중복되었는지 체크한다.
	   * ---------------------------------------------------------------------------------------- 
	   */
	  findByEmail: function() {
		  this.buttonDisabled = false;
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  data : { email: this.member.email },
				  url : '/portal/findByEmail',
				  success : function(response) {
					  console.log("success");
					  console.log(response);
					  if(response.data.message == null) {
						  //alert("Email doesn't exist.")
						  console.log("Email doesn't exist.");
						  
					  }else {
						  //alert("Email exists. Choose another email.")
						  console.log("Email exists.");
						  self.control.desc.email = "Your email address already exists. Change another one.";
						  //self.member.email = "";
						  self.control.disabled.button = true;
					  }
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  }
  }
} //export 
