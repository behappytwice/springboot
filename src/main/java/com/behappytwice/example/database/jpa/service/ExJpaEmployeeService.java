package com.behappytwice.example.database.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.behappytwice.framework.base.component.BaseService;
import com.behappytwice.example.database.jpa.entity.ExJpaEmployeeBean;
import com.behappytwice.example.database.jpa.repository.ExJpaEmployeeRepository;

@Service
public class ExJpaEmployeeService extends BaseService {

	@Autowired
	private ExJpaEmployeeRepository jpaEmployeeRepository;

	// find section

	public void findTest(String action) {
		if (action.equals("findByUserId")) {
			ExJpaEmployeeBean emp = this.jpaEmployeeRepository.findByUserId("361cfa4ba91141cfae856be11768634f");
			logger.debug("Name:" + emp.getName());
		} else if (action.equals("findByName")) {
			ExJpaEmployeeBean emp = this.jpaEmployeeRepository.findByName("박신혜");
			logger.debug("Name:" + emp.getName());
		} else if (action.equals("findByNameAndDesignation")) {
			ExJpaEmployeeBean emp = this.jpaEmployeeRepository.findByNameAndDesignation("박신혜", "부장");
			logger.debug("Name:" + emp.getName());
		} else if (action.equals("findByNameOrDesignation")) {
			List<ExJpaEmployeeBean> emp = this.jpaEmployeeRepository.findByNameOrDesignation("박신혜", "사장");
			for (ExJpaEmployeeBean e : emp) {
				logger.debug("Name:" + e.getName());
			}
		} else if (action.equals("findBySalaryLessThan")) {
			List<ExJpaEmployeeBean> emp = this.jpaEmployeeRepository.findBySalaryLessThan(700);
			for (ExJpaEmployeeBean e : emp) {
				logger.debug("Name:" + e.getName());
			}
		} else if (action.equals("findAll")) {
			List<ExJpaEmployeeBean> emp = this.jpaEmployeeRepository.findAll();
			for (ExJpaEmployeeBean e : emp) {
				logger.debug("Name:" + e.getName());
			}
		}
	}// :
	
	public void insertEmployee(ExJpaEmployeeBean employee) {
		this.jpaEmployeeRepository.save(employee);
	}//:
	public void deleteEmployee(ExJpaEmployeeBean employee) {
		this.jpaEmployeeRepository.delete(employee);
	}//:
	public void updateEmployee(ExJpaEmployeeBean employee) {
		ExJpaEmployeeBean emp = this.jpaEmployeeRepository.findByUserId(employee.getUserId());
		emp.setName("변경한");
		this.jpaEmployeeRepository.save(emp);
	}//:
	
	public void findAllEmployees() {
		List<ExJpaEmployeeBean> emp = this.jpaEmployeeRepository.findAllEmployees();
		for (ExJpaEmployeeBean e : emp) {
			logger.debug("Name:" + e.getName());
		}
	}//:
	public void findEmployeeById() {
		ExJpaEmployeeBean emp = this.jpaEmployeeRepository.findEmployeeById("7326160c9b3d4e8198a944e7a901b426");
		logger.debug("Name:" + emp.getName());
	}//:
	public void findEmployeeById2() {
		ExJpaEmployeeBean emp = this.jpaEmployeeRepository.findEmployeeById("7326160c9b3d4e8198a944e7a901b426");
		logger.debug("Name:" + emp.getName());
	}//:

	
}// :
