package com.behappytwice.framework.codec;

import static org.junit.Assert.*;

import org.junit.Test;

/** DataConverter 테스트 케이스이다. */
public class DataConverterTest {

	/**
	 * HexConverter 테스트 케이스이다.
	 */
	@Test
	public void testHexConverter() {
		
		String one = "a";
		// 문자열을 Hex  문자열로 변환 
		String hex = DataConverter.getHexConverter().bytesToHex(one.getBytes());
		System.out.println(hex);
		
		
		// Hex 문자열을 원래 문자열의 바이트 배열로 변환 
		byte[] middleStr = DataConverter.getHexConverter().hexToBytes(hex);
		System.out.println(new String(middleStr));
		
		
	}//:

}//~
