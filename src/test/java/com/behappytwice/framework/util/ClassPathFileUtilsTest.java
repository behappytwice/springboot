package com.behappytwice.framework.util;

import java.io.File;
import java.util.List;

import org.junit.Test;

public class ClassPathFileUtilsTest {

	/** 파일의 내용을 읽어서 문자열로 반환한다 */
	@Test
	public void testReadFile() throws Exception {
		String path = "allow-origin.json";
		String contents = ClassPathFileUtils.readFile(path);
		System.out.println(contents);
	}//:
	
	/** 파일의 내용을 읽어서 List<String>으로 반환한다 */
	@Test
	public void testReadFileToList() throws Exception {
		String path = "application-dev.properties";
		List<String> contents = ClassPathFileUtils.readFileToList(path);
		contents.forEach(item ->  System.out.println(item) );
	}//:
	
	/** 클래스패스에 있는 파일정보들 전달받아 File 객체를 반환한다.*/
	@Test
	public void testGetFileObject() throws Exception {
		String path = "application-dev.properties";
		File file = ClassPathFileUtils.getFileObject(path);
		System.out.println(file.getAbsolutePath());
	}//:
	
	
	/** 클래스패스에 있는 파일정보들 전달받아 File 객체를 반환한다.*/
	@Test
	public void testGetFileObject2() throws Exception {
		String path = "application.properties";
		File file = ClassPathFileUtils.getFileObject(path);
		System.out.println(file.getAbsolutePath());
	}//:
}///~
