package com.behappytwice.example.database.jpa.repository;

import java.util.List;

import com.behappytwice.example.database.jpa.entity.ExJpaEmployeeBean;

/** EntityManager를 사용하기 위한 Custom Interface */
public interface ExJpaEmployeeRepositoryCustom {

	List<ExJpaEmployeeBean> myList();
	List<ExJpaEmployeeBean> selectEmployeeTest();
	List<ExJpaEmployeeBean> selectEmployeeTest2();
	ExJpaEmployeeBean selectSingleEmployee();
}
