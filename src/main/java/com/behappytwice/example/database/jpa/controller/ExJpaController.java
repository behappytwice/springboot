package com.behappytwice.example.database.jpa.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.behappytwice.framework.base.component.BaseController;
import com.behappytwice.framework.util.IDGenerator;
import com.behappytwice.example.database.jpa.entity.ExJpaEmployeeBean;
import com.behappytwice.example.database.jpa.service.ExJpaEmployeeService;

@Controller
@RequestMapping(value="/example/jpa")
public class ExJpaController extends BaseController {
	
	@Autowired
	private ExJpaEmployeeService jpaEmployeeService;
	
	@RequestMapping(value="findTest") 
	public String findTest(@RequestParam("action") String action) {
		this.jpaEmployeeService.findTest(action);
		return "index";
	}
	
	
	@RequestMapping(value="/insertEmployees")
	public String insertEmployees() throws Exception {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		String[] names = new String[] { "홍길동", "박찬호", "박신혜", "민주희", "강영민", "유준상", "윤석열", "지민희", "박영신", "리콜" };
		String[] desig = new String[] { "사장", "이사", "부장", "사원", "주임", "대리", "전무", "사원", "과장", "주임" };
		int[] salaries = new int[] { 100, 200, 300, 305, 300, 500, 600, 700, 500 , 800};
		Date[] dates = new Date[] { 
				 format.parse("2010/03/05")
				,format.parse("2014/05/05")
				,format.parse("2017/04/15")
				,format.parse("2010/03/05")
				,format.parse("2010/03/05")
				,format.parse("2016/08/01")
				,format.parse("2018/04/02")
				,format.parse("2015/05/03")
				,format.parse("2019/06/04")
				,format.parse("2011/07/05")
				};
				
		for(int i=0; i < 10; i++) {
			ExJpaEmployeeBean employee = new ExJpaEmployeeBean();
			employee.setUserId(IDGenerator.generateID());
			employee.setName(names[i]);
			employee.setDesignation(desig[i]);
			employee.setSalary(salaries[i]);
			employee.setEnterDate(dates[i]);
			this.jpaEmployeeService.insertEmployee(employee);
		}
		
		
//		List<JpaEmployee> list = this.jpaEmployeeService.myList();
//		JpaEmployee emp = list.get(0);
//		
//		logger.debug(">>>>>>>>>>> employee's name=" + emp.getName());
//		logger.debug(">>>>>>>>>>> employee's name=" + emp.getName());
		
//		List<Long> list = this.jpaEmployeeService.selectUserCount();
//		Long count = list.get(0);
//		logger.debug("User Count:" + count);
//		
//		
//		this.jpaEmployeeService.selectEmployeeTest();
//		this.jpaEmployeeService.selectEmployeeTest2();
//		logger.debug(">>>>>>>>>>>>>>> Succeeded");
//		
//		this.jpaEmployeeService.selectSingleEmployee();
		
		return "index";
	}
	

	@RequestMapping(value="/insertEmployee")
	public String insertEmployee() throws Exception {
		ExJpaEmployeeBean employee = new ExJpaEmployeeBean();
		employee.setUserId(IDGenerator.generateID());
		employee.setName("홍길동");
		employee.setDesignation("부장");
		employee.setSalary(500);
		employee.setEnterDate(new Date());
		this.jpaEmployeeService.insertEmployee(employee);
		return "index";
	}
	
	@RequestMapping(value="/deleteEmployee")
	public String deleteEmployee()  {
		ExJpaEmployeeBean employee = new ExJpaEmployeeBean();
		employee.setUserId("2ba78f137e714a0e8e895abfff477d60");
		this.jpaEmployeeService.deleteEmployee(employee);
		return "index";
	}//:
	
	@RequestMapping(value="/updateEmployee")
	public String updateEmployee()  {
		ExJpaEmployeeBean employee = new ExJpaEmployeeBean();
		employee.setUserId("61b3bedabeac4211847625b977d85842");
		this.jpaEmployeeService.updateEmployee(employee);
		return "index";
	}//:

	@RequestMapping(value="/findAllEmployees")
	public String findAllEmployees()  {
		this.jpaEmployeeService.findAllEmployees();
		return "index";
	}//:
	
	@RequestMapping(value="/findEmployeeById")
	public String findEmployeeById()  {
		this.jpaEmployeeService.findEmployeeById();
		return "index";
	}//:
	@RequestMapping(value="/findEmployeeById2")
	public String findEmployeeById2()  {
		this.jpaEmployeeService.findEmployeeById2();
		return "index";
	}//:
	
	
}////~
