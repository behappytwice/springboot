import Ajax from '/framework/js/Ajax.js';
import EventBus from '/common/js/EventBus.js';

export default {
  template: `
     <div>
			<b-list-group>
			  <b-list-group-item @click="onListGroupItemClicked" class="mouse-pointer">작성하기</b-list-group-item>
			  <b-list-group-item>글목록</b-list-group-item>
			</b-list-group>
     </div>
  `,
  // constructor section 
  created() {
	  
  },
  // data section 
  data() {
    return {
    }
  },
  // method section 
  methods: {
	  onListGroupItemClicked : function() {
		  //EventBus.$emit('EVT_LEFT_MENU_CLICKED', 'WRITE');
		  this.$emit('EVTC_MENU_CLICKED', 'WRITE');
	  }
  }
} //export 
