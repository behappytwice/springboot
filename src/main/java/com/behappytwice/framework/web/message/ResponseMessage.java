package com.behappytwice.framework.web.message;

import lombok.Getter;
import lombok.Setter;


/**
 * XMLHTTPRequest 요청에 대한 응답 객체
 * @author behap
 *
 */
@Getter
@Setter
public class ResponseMessage {
	/** 응답 코드 */
	private String status;
	/** 응답 메시지 */
	private String statusText;
	/** 상세한 응답 메시지 */
	private String description;
	/** 응답 데이터 */
	private Object message;
	

}///~
