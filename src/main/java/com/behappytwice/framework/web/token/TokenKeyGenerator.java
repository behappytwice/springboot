package com.behappytwice.framework.web.token;

import java.security.Key;
import java.util.Base64;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

/**
 * JWT Token을 사용할 때 사인을 하기 위한 Key를 생성한다. 키는 별도로 보관하고 
 * Token을 Sign 하거나 Unsign할 때 사용한다. 
 * @author behap
 *
 */
public class TokenKeyGenerator {
	/**
	 * 토큰 생성을 위한 Key를 생성한다.
	 * 
	 * @return Base64 encoded된 문자열
	 */
	public static String createKey() {
		// JWS 쓰기
		// 알고리즘 선택
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		// 키 생성
		Key key = Keys.secretKeyFor(signatureAlgorithm);
		// 키 바이트로 변환
		byte[] encodedKey = key.getEncoded();
		// base64로 변환
		String encodedString = Base64.getEncoder().encodeToString(encodedKey);
		return encodedString;
	}// :
	
	/**
	 * Token Key를 생성한다.
	 * @param args
	 */
	public static void main(String[] args) {
		String encodedString = TokenKeyGenerator.createKey();
		System.out.println("아래 키를 application.properties 파일에 jwt.key라는 이름으로 저장한다.");
		System.out.println(encodedString);
	}// :
	
}///~
