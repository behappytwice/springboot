package com.behappytwice.app.biz.board.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.behappytwice.app.biz.base.entity.Entry;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="article")
@Getter
@Setter
public class Article extends Entry {

	@Column(name="content")
	protected String content;
	@Column(name="reply_count")
	protected int replyCount;
	
	
}///~
