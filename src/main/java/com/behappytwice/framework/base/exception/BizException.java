package com.behappytwice.framework.base.exception;


/**
 * 시스템 오류가 아닌 업무로직에서 발생한 예외를 처리할 때 사용한다. 
 * 
 * @author behap
 *
 */
public class BizException extends RuntimeException {
	
	/** 오류 코드 */
	private String code;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * 생성자
	 * @param code 오류코드 
	 * @param message  오류메시지 
	 */
	public BizException(String code, String message) {
		super(message);
		this.code = code;
	}
}
