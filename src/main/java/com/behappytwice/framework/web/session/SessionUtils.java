package com.behappytwice.framework.web.session;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.behappytwice.framework.constants.FrameworkConstants;

/**
 * HttpSession을 처리하기 위한 Utility 클래스이다.
 * @author behap
 *
 */
public class SessionUtils {
	
	/**
	 * 0(zero) 또는 음수인 경우에는 session이 timeout되지 않음, 단위는 초.
	 * 세션 유지시간은 브라우저의 요청이 있을 때마다 갱신 
	 */
	public static final int MAX_INACTIVE_INTERVAL = ( 60 * 60 * 8); 

	/**
	 * 세션을 반환한다. 세션이 없으면 새로 생성한다.
	 * 
	 * @param request requst 객체  
	 * @param sessionTimeout  타임아웃 시간(초)
	 * @return
	 * 		HttpSession 
	 * 		
	 */
	public static HttpSession getSession(HttpServletRequest request, int sessionTimeout) {
		// true : 세션이 없다면 생성한다. 
		HttpSession session = request.getSession(true);
		session.setMaxInactiveInterval(sessionTimeout);
		return session;
	}//:
	
	
	/**
	 * 세션을 반환한다. 세션이 없으면 새로 생성한다.
	 * @param request request 객체 
	 * @return
	 * 		HttpSession
	 */
	public static HttpSession getSession(HttpServletRequest request) {
		return getSession(request, MAX_INACTIVE_INTERVAL);
	}//:

	/**
	 * 세션 아이디를 반환한다.
	 * @param request HttpServletRequst
	 * @return
	 * 		세션아이디
	 */
	public static String getSessionID(HttpServletRequest request) {
		HttpSession sess = getSession(request);
		return sess.getId();
	}
	
	/**
	 * 세션을 무효화 시킨다. 로그아웃 처리할 때 사용 
	 * @param request
	 */
	public static void invalidate(HttpServletRequest request) {
		getSession(request).invalidate();
	}//:
	
	/**
	 * 세션을 무효화 한다.
	 */
	public static void invalidate() {
		invalidate(getHttpServletRequst());
	}//:
	
	
	/**
	 * 세션에 객체를 저장한다.
	 * 
	 * @param request HttpServletRequest
	 * @param key 세션에 담을 데이터의 키 
	 * @param data 세션에 담을 데이터
	 */
	public static void setObject(HttpServletRequest request, String key, Object data) {
		HttpSession session = getSession(request);
		synchronized (session) {
			session.setAttribute(key, data);
		}
	}//:
	
	/**
	 * 세션에 저장된 데이터를 반환한다.
	 * @param request HttpServletRequest
	 * @param key 세션에 데이터를 담을 때 사용한 키
	 * @return
	 * 		세션에 저장된 데이터
	 */
	public static Object getObject(HttpServletRequest request, String key) {
		Object obj = null;
		HttpSession session = getSession(request);
		synchronized (session) {
			obj = session.getAttribute(key);
		}
		return obj;
	}//:
	
	
	/**
	 * 로그인 사용자 객체를 반환한다. 
	 * @return
	 */
	public static Object getSignedUser() {
		HttpServletRequest request = getHttpServletRequst();
		return getObject(request, FrameworkConstants.SESSION_SIGNIN_USER_KEY);
	}//:
	
	/**
	 * 로그인 여부를 판단한다. 
	 * @param request HttpServletRequest
	 * @return
	 * 		세션에 로그인된 사용자 정보가 있은 true, 아니면 false를 반환한다.
	 */
	public static boolean isSigned() {
		HttpServletRequest request = getHttpServletRequst();
		Object o = getObject(request, FrameworkConstants.SESSION_SIGNIN_USER_KEY);
		return (o == null) ? false : true; 
	}//:
	

	/**
	 * 로그인 사용자의 정보를 세션에 넣는다.
	 * @param signedUser 사용자 정보 
	 */
	public static void setSignedUser(Object signedUser) {
		HttpServletRequest request = getHttpServletRequst();
		setObject(request,FrameworkConstants.SESSION_SIGNIN_USER_KEY, signedUser );
	}//:

	/**
	 * ThreadLocal에 담겨 있는 HttpServletRequest 객체를 반환한다.
	 * @return 
	 * 		HttptServletRequest
	 */
	private static HttpServletRequest getHttpServletRequst() {
		Map map = SessionThreadLocal.getValue();
		return (HttpServletRequest)map.get(FrameworkConstants.THREAD_LOCAL_HTTP_REQUEST_KEY);
	}//:
	
}////~
