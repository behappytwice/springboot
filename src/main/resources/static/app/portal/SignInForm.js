import Ajax     from '/framework/js/Ajax.js';
import Utils    from '/framework/js/Utils.js';
import EventBus from '/framework/js/EventBus.js';

export default {
  template: `
     <div>
        <b-form>
              <h3>Sign In</h3>
              <hr>
              <label for="member-email">Email</label>
		      <b-input type="email"  v-model="member.email" id="member-email"   required></b-input>
              <label for="member-password">Password</label>
		      <b-input type="password" v-model="member.password"  id="member-password"></b-input>
		      <hr>
		      <h6>{{signInMessage}}</h6>
       </b-form>
       <br>
       <b-button variant="success" @click="onButtonClick">Login</b-button>
     </div>
  `,
    // data section 
  data() {
    return {
    	signInMessage : "",
    	message : {
    	},
    	control : {
    		// 화면요소를 enable하거나 disabled하기 위한 변수들
    		status : { 
    		}
    	},
    	// Entity
    	member : {
    		name: "",
    		email:"",
    		email2: "",
    		password:""
    	}
    }
  },
  // constructor section 
  created() {
  },
  mounted() {
  },
  computed : {
  },
  // method section 
  methods: {
	  onButtonClick : function(){
		  if(Utils.isEmpty(this.member.email) || Utils.isEmpty(this.member.password)) {
			  return; 
		  }
		  this.signIn();
	  },
	  // login
	  signIn : function() {
		  
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  data : this.member,
				  url : '/portal/signin',
				  success : function(response) {
					  console.log("success");
					  console.log(response);
					  if(response.data.status == "200") {
						  console.log("signin succeed.");
						  self.signInMessage = "";
						  // Redirects the main screen 
						  document.location.href = "/main/main";
					  }else {
						  console.log("signin failed");
						  console.log(response.data.statusText);
						  self.signInMessage = response.data.statusText
					  }
				  },
				  fail: function(error) {
					  //alert("Error occured:" + error.message);
					  console.log("Internal Server Error");
				  }
		  };
		  new Ajax().ajax(requestConfig); 
	  }//signIn
  }
} //export 
