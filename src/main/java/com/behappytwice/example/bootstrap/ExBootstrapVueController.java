package com.behappytwice.example.bootstrap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.behappytwice.framework.base.component.BaseController;

/**
 * BootStrapVue Component 테스트를 위한 컨트롤로이다.
 * @author behap
 *
 */
@Controller
@RequestMapping(value="/example/boot")
public class ExBootstrapVueController extends BaseController {
	
	/** 메인화면 */
	@RequestMapping(value="/main")
	public String main()  {
		return "sample/bootstrapvue/BootstrapMain";
	}//:
	
	
}
