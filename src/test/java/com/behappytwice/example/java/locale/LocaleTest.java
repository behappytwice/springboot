package com.behappytwice.example.java.locale;

import java.util.Arrays;
import java.util.Locale;

import org.junit.Test;

public class LocaleTest {

	
	@Test
	public void test() {
		Locale locale = new Locale("ko", "KR");
		System.out.println("getDisplayLanguage:" + locale.getDisplayLanguage());
		System.out.println("getLanguage:" + locale.getLanguage());
		System.out.println("getDisplayCountry:" + locale.getDisplayCountry());
		System.out.println("getCountry:" + locale.getCountry());
	}
	
	@Test
	public void test2() {
		Arrays.stream(Locale.getAvailableLocales()).forEach( locale -> {
			System.out.println("getLanguage:" + locale.getLanguage() + "//" + "getCountry:" + locale.getCountry());
		});
	}//

	@Test
	public void test3() {
	}//:
	
}//~
