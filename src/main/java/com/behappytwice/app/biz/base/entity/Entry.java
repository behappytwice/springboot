package com.behappytwice.app.biz.base.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="entry")
@Getter
@Setter
@Inheritance(strategy=InheritanceType.JOINED)
public class Entry extends TimeAndAccessorInfo {
	
	@Id
	@Column(name="entry_id")
	protected String entryId;
	@Column(name="subject")
	protected String subject;
	@Column(name="entry_type")
	protected String entryType;
	
}///~
