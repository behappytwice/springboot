package com.behappytwice.framework.json;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * java.util.Date 타입의 필드의 값을 IOS형식의 문자열로 변환하기 위해서 사용한다.
 * @author behap
 *
 */
public class DateDeserializer extends StdDeserializer<Date> {
	private String formatString = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	
	private SimpleDateFormat formatter =    new SimpleDateFormat(formatString);
	
	public DateDeserializer() {
		this(null);
	}
	public DateDeserializer(Class<Date> vc) {
		super(vc);
	}
	
	@Override
	public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		String date = p.getText();
		try { 
			return formatter.parse(date);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
		 
}////~
