import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';
import MainTop from './MainTop.js';
import MainContent from './MainContent.js';

new Vue( {
  el: '#app',
  template: `
     <div>
          <b-container>
	           <b-row>
	               <b-col>
	                   <main-top></main-top>
	                   <hr>
	               </b-col>
	           </b-row>
	           <b-row>
	               <b-col sm="3">
	                   left
	               </b-col>
	               <b-col sm="6">
	                  <main-content></main-content>
	               </b-col>
	               <b-col sm="3">
	                  right
	               <b-col>
	           </b-row>
            </b-container>
            <b-modal id="modal-callback" title="정보" ok-only>
               <p class="my-4">처리 되었습니다.</p>
            </b-modal>
     </div>
  `,
  
  components : {
	  MainTop,
	  MainContent
  },
  // constructor section 
  created() {
  },
  // data section 
  data() {
    return {
    }
  },
  // method section 
  methods: {
  }
}); //export 
