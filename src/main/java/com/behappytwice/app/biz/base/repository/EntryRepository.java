package com.behappytwice.app.biz.base.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.behappytwice.app.biz.base.entity.Entry;

public interface EntryRepository extends JpaRepository<Entry, String> {

	@Modifying
	@Query(value="UPDATE entry SET subject = :subject , last_modified_time = :lastmodifiedtime where entry_id = :entryid" , nativeQuery=true)
	void updateSubject(@Param("subject") String subject
			, @Param("lastmodifiedtime") Date lastModifiedTime
			, @Param("entryid") String entryId );
	
	Entry findByEntryId(String entryId);
	
}
