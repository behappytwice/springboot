var Child = {
		template : `
		   <div>
		   
		       <header>
    		       <slot name="header" v-bind:user="user">
   		              {{user.lastName}}
		            </slot>
		            <slot>이건 이름이 없어서 디폴트 슬롯</slot>
		       </header>

		   </div>
		`,
		props: ['message'],
		data : function() {
			return {
				user : {
					firstName:"Andy",
					lastName: "Kim"
				}
			}
		},
		methods : {
		}
}

new Vue({
  el: '#app',
  template : `
	     <div>
	        <child>
  	            <template v-slot:header="slotProps" >
	               <h1>{{slotProps.user.firstName}} </h1>
	           </template>
	           <h3>이건 디폴트 슬롯에 들어감</h3>
	        </child>
	        <div>부모</div>
         </div>
  `,
  created() {
  },
  components: {
	  child:Child
  },
  
  data() {
	  return {
	  }
  },
  methods : {
  }
});
