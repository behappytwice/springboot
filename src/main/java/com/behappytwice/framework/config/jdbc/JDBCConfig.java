package com.behappytwice.framework.config.jdbc;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.behappytwice.framework.log.Log;




/**
 * DataSource, SqlSesionFactory, SqlSession, TransactionManager 등 JDBC 사용을 위한 Bean을 생성한다.
 * 하나 이상의 DB에 접근하기 위해서는 DataSource 빈을 하나 이상 생성해야 한다. 구분을 위해 메인이 되는 빈들은
 * Primary 어노테이션을 적용하고, Bean 어노테이션을 설정할 때 name 속성을 사용하여 이름을 반드시 설정해야 한다.
 * 
 * @author behap
 *
 */
@Configuration
@EnableTransactionManagement
public class JDBCConfig {

	private static @Log Logger LOG;
	
	@Autowired
	private Environment env;
	
	/**
	 * DataSource를 생성한다.
	 * 
	 * @return
	 */
	@Primary
	@Bean(name = "dataSource")
	@ConfigurationProperties(prefix = "spring.datasource.hikari")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}
	
	
	/**
	 * JdbcTemplate을 생성한다.
	 * 
	 * @param dataSource
	 * @return
	 */
	@Bean("jdbcTemplate")
	public JdbcTemplate jdbcTemplate(@Autowired @Qualifier("dataSource") DataSource dataSource){
	    return new JdbcTemplate(dataSource);
	}//:
	
	
	/**
	 * TransactionManager를 생성한다. 
	 * 
	 * @param dataSource
	 * @return
	 */
	@Primary
	@Bean(name = "transactionManager")
	public DataSourceTransactionManager transactionManager(@Autowired @Qualifier("dataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}//:
	
}///~

