package com.behappytwice.framework.util.xml;

import org.junit.Test;

import com.behappytwice.example.model.ExUserBean;


/** XmlUtils.java Test Case이다. */
public class XmlUtilsTest {


	/** 객체를 XML로 변환한다. */
	@Test
	public void testMarshal() {
		ExUserBean bean = new ExUserBean();
		bean.setUserId("bluesky");
		bean.setUserName("Korea");
		String xml = XmlUtils.marshal(ExUserBean.class, bean);
		System.out.println(xml);
//		<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
//		<sampleUserBean>
//		    <userId>bluesky</userId>
//		    <userName>Korea</userName>
//		</sampleUserBean>
		
	}//:
	
	
	/** XML을 객체로 변환한다 .*/
	@Test
	public void testUnmarshal() {
		String xml = ""
		+ "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
		+ "<sampleUserBean>  "
		+ "    <userId>bluesky</userId> "
		+ "    <userName>Korea</userName> "
		+ "</sampleUserBean>";
		ExUserBean bean = (ExUserBean)XmlUtils.unmarshal(xml, ExUserBean.class);
		System.out.println(bean.getUserName());
	}//:
	

	/** 객체를 XML로 변환하여 반환한다 */
	@Test
	public void testToXML() {
		ExUserBean bean = new ExUserBean();
		bean.setUserId("bluesky");
		bean.setUserName("Korea");
		String xml = XmlUtils.toXML("user", ExUserBean.class, bean);
		System.out.println(xml);
//		<user>
//		  <userId>bluesky</userId>
//		  <userName>Korea</userName>
//		</user>
	}//:
	
	/** XML를 객체로 변환한다. */
	@Test
	public void testToObject() {
		String xml = ""
		+ "<user>  "
		+ "    <userId>bluesky</userId> "
		+ "    <userName>Korea</userName> "
		+ "</user>";
		ExUserBean bean = (ExUserBean)XmlUtils.readXmlToObjectTest("user", ExUserBean.class, xml);
		System.out.println(bean.getUserName());
		
	}//:
	
}///~
