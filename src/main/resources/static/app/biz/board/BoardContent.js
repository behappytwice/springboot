import Ajax from '/framework/js/Ajax.js';
import EventBus from '/common/js/EventBus.js';
import BoardList from './BoardList.js';

export default {
  template: `
     <div>
         <board-list></board-list>
     </div>
  `,
  // constructor section 
  created() {
  },
  components: {
	BoardList : BoardList  
  },
  // data section 
  data() {
    return {
    }
  },
  // method section 
  methods: {
	  
  }
} //export 
