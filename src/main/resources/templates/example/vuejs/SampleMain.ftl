<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<#include "/framework/include/BootStrapCss.ftl">
<link rel="stylesheet" type="text/css" href="/sample/vuejs/css/main.css">
</head>
<body>

<div id="app">
</div>

<script type="module" src="/example/vuejs/component/SampleMain.js"></script> 

<#include "/framework/include/CommonJavaScript.ftl">
</body>
</html>