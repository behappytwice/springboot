<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<#include "/framework/include/BootStrapCss.ftl">

<style>

  .main-card-title {
    font-size : 1.2em;
    font-weight: bold;
    color:#526E26
  }
  
  .main-card-answer {
     color:#898E82 ;
  }
  .main-card-author {
      color:#23D5F1 ; 
  }
  .main-card-updated {
      color:red;
  }
  .main-card-content  {
      color:black;
  }
  
</style>
</head>
<body>

<div id="app"> 
</div>


<script type="module" src="/app/portal/Main.js"></script> 

<#include "/framework/include/CommonJavaScript.ftl">

</body>
</html>