import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';


Vue.directive('orange',	function(el, binding, vnode) {
		el.style.color = "orange";
});
Vue.directive('highlight',	function(el, binding, vnode) {
	el.style.color = binding.value;
});



new Vue({
  el: '#app',
  template : `
	     <div>
	        {{message}}
	        <div v-bind:id="id">{{id}}</div>
	        <div :id="id">{{id}}</div>
	        <span v-orange>이 부분이 orange 색으로 표시된다.</span>
	        <b-button>Button</b-button>
	        <span v-highlight="'red'">이 부분이 red 색으로 표시된다.</span>
         </div>
  `,
  created() {
  },
  components: {
  },
  data() {
	  return {
		  id: "abc-123",
		  message : "안녕하세요",
		  showDismissibleAlert: false
	  }
  },
  methods : {
	
  }
});
