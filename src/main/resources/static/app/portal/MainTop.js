import Ajax     from '/framework/js/Ajax.js';
import Utils    from '/framework/js/Utils.js';
import EventBus from '/framework/js/EventBus.js';

export default {
  template: `
     <div>
		  <b-nav>
		    <b-nav-item active>Active</b-nav-item>
		    <b-nav-item>Link</b-nav-item>
		    <b-nav-item>Another Link</b-nav-item>
		    <b-nav-item @click="onSignoutClicked">Sign out</b-nav-item>
		  </b-nav>
     </div>
  `,
    // data section 
  data() {
    return {
    }
  },
  // constructor section 
  created() {
  },
  mounted() {
  },
  computed : {
  },
  // method section 
  methods: {
	  onSignoutClicked : function(event) { 
		  this.signOut();
	  },
	  // log out 처리 
	  signOut : function() {
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  url : '/main/signout',
				  success : function(response) {
					  if(response.data.status == "200") {
						  alert("Logout 되었습니다.");
						  document.location.href = "/portal/portal";
					  }else {
						  alert("logout 실패");
					  }
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Ajax().ajax(requestConfig);
	  }
  }
} //export 
