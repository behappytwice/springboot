package com.behappytwice.app.biz.organization.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.behappytwice.app.biz.organization.entity.Admin;

public interface AdminRepository extends JpaRepository<Admin, String> {

}
