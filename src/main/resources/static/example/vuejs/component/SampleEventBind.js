var Child = {
		template : `
		   <div>
		       <button @click='incrementCounter'>{{counter}}</button>
		       부모 컴포넌트의 메시지 : {{message}}
		   </div>
		`,
		props: ['message'],
		data : function() {
			return {
				counter: 0
			}
		},
		methods : {
			incrementCounter : function() {
				this.counter += 1;
				this.$emit('increment'); // fires an event 
			}
		}
}

new Vue({
  el: '#app',
  template : `
	     <div>
	        {{total}}
	        <a :href="hrefbind">Click Me</a>
	        <my-component @increment="incrementTotal" v-bind:message="parentMsg"></my-component>
         </div>
  `,
  created() {
  },
  components: {
	  'my-component':Child
  },
  
  data() {
	  return {
		  total : 0,
		  parentMsg:"ㅎㅎㅎㅎㅎㅎ",
		  hrefbind:"http://www.naveer.com"
	  }
  },
  methods : {
	  incrementTotal : function() {
		  this.total += 1;
	  }
  }
});
