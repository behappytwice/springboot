package com.behappytwice.example.vuejs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.behappytwice.framework.base.component.BaseController;
import com.behappytwice.example.model.ExMenu;
import com.behappytwice.example.model.ExUserBean;

/** Vue.js를 테스트하기 위한 컨트롤러 */
@Controller
@RequestMapping(value="/example/vue")
public class  ExVueController extends BaseController {
	
	/** 메인 화면 */
	@RequestMapping(value="/main")
	public String main()  {
		return "sample/vuejs/SampleMain";
	}//:
	
	/** evnet binding 시험 화면 */
	@RequestMapping(value="/eventbind")
	public String eventbind()  {
		return "sample/vuejs/SampleEventBind";
	}//:
	
	/** slot 시험 화면 */
	@RequestMapping(value="/slot")
	public String slot()  {
		return "sample/vuejs/SampleSlot";
	}//:
	

	/** 사용자 정보를 반환한다. */
	@RequestMapping(value="/getUser")
	public @ResponseBody ExUserBean getUser() {
		logger.debug("#### a request received.");
		ExUserBean data = new ExUserBean();
		data.setUserId("334400000");
		data.setUserName("홍길동");;
		return data;
	}//:
	
	/** 사용자 목록을 반환한다. */
	@RequestMapping(value="/getUserList")
	public @ResponseBody List<ExUserBean> getUserList() {
		
		List<ExUserBean> list = new ArrayList<ExUserBean>();
		ExUserBean user = new ExUserBean();
		user.setUserId("1111");;
		user.setUserName("홍길동");
		list.add(user);
		user = new ExUserBean();
		user.setUserId("222");;
		user.setUserName("홍길동2");
		list.add(user);
		user = new ExUserBean();
		user.setUserId("3333");;
		user.setUserName("홍길동3");
		list.add(user);
		
		return list;
	}

	/** 메뉴 목록을 반환한다 */
	@RequestMapping(value="/getMenus")
	public @ResponseBody List<ExMenu> getVueMenu() {
		
		List<ExMenu> list = new ArrayList<ExMenu>();
		ExMenu  menu = new ExMenu();
		menu.setManuId("1011");
		menu.setName("모든사용자");
		menu.setUrl("/vue/getUserList");
		list.add(menu);
		
//		menu = new VueMenu();
//		menu.setManuId("1012");
//		menu.setUserName("사용자정보조회");
//		menu.setUrl("/vue/getUser");
//		list.add(menu);
		
		return list;
		
	}//:
	
}
