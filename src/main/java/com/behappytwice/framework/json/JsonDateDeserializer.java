package com.behappytwice.framework.json;


import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Date 문자열을 처리하기 위한 Deserializer입니다.
 * 
 * @author Sanghyun,Kim(sanghyun@naonsoft.com)
 * @deprecated
 *  불안전한 방법이므로 사용하지 않는다.
 *
 */
public class JsonDateDeserializer extends JsonDeserializer<Date> {
	
	/**
	 * Default Dateformat
	 */
	private static String defaultDateFormat = "yyyy-MM-dd aaa hh:mm:ss";
	/**
	 * Date Format들입니다.
	 */
	private List<String> dateFormats;
	public JsonDateDeserializer(List<String> dateformats) {
		dateFormats = dateformats;
	}
	
	/**
	 * 필드를 구한다. 
	 */
	private Field getField(String fieldName, Class clazz) {
		Field fld = null;
		try {
			fld = clazz.getDeclaredField(fieldName);
			return fld; 
		}catch(Exception e) {
			if(clazz.getSuperclass() == null) {
				throw new RuntimeException("NoSuchFieldException");
			}else { 
				return getField(fieldName, clazz.getSuperclass());	
			}
		}
	}
	/**
	 * JSON Date 문자열을 java.util.Date로 변환합니다.
	 */
	@Override
	public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		
		 try { 
			 //Field field = p.getCurrentValue().getClass().getDeclaredField(p.getCurrentName());
			 Field field =  getField(p.getCurrentName(), p.getCurrentValue().getClass()); 
			 JsonFormat anno = field.getAnnotation(JsonFormat.class);
			 if( anno != null) {
				 // JsonFormat는 TimeZone을 설정하거나 사용할 수 없음. 고정되어 있음
				 SimpleDateFormat sdf = new SimpleDateFormat(anno.pattern(), Locale.ENGLISH);
				 if(p.getText() == null || p.getText().equals("")) { 
					 return null;
				 }else { 
					return  sdf.parse(p.getText().toString());	 
				 }
			 }
		 }catch(Exception e) {
			 // 여기서는 에러처리하지 않음
			 // e.printStackTrace();
		 }
		 
		 if (p.getCurrentToken().equals(JsonToken.VALUE_STRING)) { 
			 if(p.getText() == null || p.getText().equals(""))  {
				 return null;
			 }
			 String value = p.getText().toString();
			 SimpleDateFormat sf = null; 
			 try { 
					if (value.matches("\\d{4}-\\d{2}\\-\\d{2}")) {
						sf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
						return sf.parse(value);
					} else if (value.matches("\\d{4}/\\d{2}\\/\\d{2}")) {
						sf = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
						return sf.parse(value);
					} else if (value.matches("\\d{8}")) {
						sf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
						return sf.parse(value);
					} else if (value.matches("\\d{14}")) {
						sf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
						return sf.parse(value);
					} else if (value.matches("\\d{4}/\\d{2}\\/\\d{2}")) {
						// "yyyy-MM-dd hh:mm" 
						sf = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
						return sf.parse(value);
					}else if(value.matches("\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}")) {
						System.err.println("slash and date hour minute match");
						sf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
						return sf.parse(value);
					} else {

						try {
							// Supports java 8 or over
							CharSequence cs = value;
							Instant instant = Instant.parse(cs);
							return Date.from(instant);
						}catch(Exception e1) {
							 if(dateFormats == null || dateFormats.size() == 0) {
								 sf = new SimpleDateFormat(defaultDateFormat, Locale.ENGLISH);
								 try { 
									 Date d=  sf.parse(p.getText().toString());
									 return d;
								 }catch(Exception e) { 
									 return null;
								 }
							}else { 
								for(String dateFormat: dateFormats) {
									try { 
										sf = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
										Date d =  sf.parse(p.getText().toString());
										return d;
									}catch(Exception e) { 
										// Ignores this exception 
									}
								}// for
							}// if
						}
					}		 
			 }catch(Exception e) {
				 return null;
			 }
		 }
		 return null;
	}

}
