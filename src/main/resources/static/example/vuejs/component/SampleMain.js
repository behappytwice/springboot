import Ajax from '/framework/js/Ajax.js';
import EventBus from '/framework/js/EventBus.js';
import Greeting from './sub/sub_main.js';
import SampleLeft from './SampleLeft.js';
import SampleContent from './SampleContent.js';
import SampleList from './SampleList.js';
import TestVar from '/framework/js/TestVar.js';

new Vue({
  el: '#app',
  template : `
	     <div>
	         <div class="top">top{{user.name}}</div>
	         <div class="container">
	            <sample-left></sample-left>
	            <component v-bind:is="currentView"></component>
	         </div>
	         <div class="bottom">Copyright(c) BeHappyTwice</div>
         </div>
  `,
  created() {
	  this.getUser();
	  EventBus.$on('EVENT_LEFT_MENU_CLICKED', this.onLeftMenuClicked);
	  EventBus.$on('EVENT_LIST_ITEM_CLICKED', this.onListItemClicked);
  },
  components: {
	  SampleLeft,
	  SampleContent,
	  SampleList
  },
  data() {
	  return {
		  currentView:SampleList, // 현재 view
		  user: {},
		  hello : ""
	  }
  },
  methods : {
	  onListItemClicked : function(userId) {

		  // currentView에 컴포넌트를 변경하고
		  // EventBus.$emit() 이벤트를 발생하면 
		  // SampleContent가 아직 created() 된 것이 아니라 이벤트를 받지 못함
		  // 어쩔 수 없이 setTimeout을 사용했는데, 더 늦게 생성되는 경우도 있을 것 같음 
		  // 다른 방법을 찾아봐야 함
		  this.currentView = SampleContent;
		  setTimeout(function(){ EventBus.$emit("EVENT_GET_USER", userId); }, 200);
	  },
	  onLeftMenuClicked: function(url) {
		  if(url == '/vue/getUserList') {
			  this.currentView = SampleList;
			  EventBus.$emit("EVENT_GET_USERLIST");
		  }
	  },
	  getUser: function() {
		  var self = this; // this 저장해 놓아야 함 
		  var requestConfig = {
				  method : 'post',
				  url : '/vue/getUser',
				  success : function(response) {
					  self.user = response.data; // 저장해 놓은 self 사용
				  },
				  fail: function(error) {
					  alert("Error occured:" + error.message);
				  }
		  };
		  new Utils().ajax(requestConfig);
	  }//:
  }
});
