package com.behappytwice.framework.base.component;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import com.behappytwice.framework.base.BaseBean;
import com.behappytwice.framework.web.message.ResponseMessage;

/**
 * 모든 Controller 클래스의 슈퍼클래스이다. Controller를 구현할 때 이 클래스를 상속 받아야 한다. 
 * @author behap
 *
 */
@Controller
public class BaseController extends BaseBean {
	
	
	public boolean assertNullOrNullString(HttpServletRequest request, String parameterName) {
		return StringUtils.isEmpty(request.getParameter(parameterName));
	}//
	
	
	/// 나중에 고치자 
	public boolean assertNull(Object object, String fieldName) {
		try {
			Field field = object.getClass().getField(fieldName);
			ReflectionUtils.getField(field, object);
			return false;
		}catch(Exception e) {
			return true; 
		}
	}//:
	
	
	/**
	 * 클라이언트 요청이 성공적으로 처리되었을 때 JSON으로 결과를 돌려주기 위해서는 ResponseMessage를 사용해야 한다.
	 * 이 메소드를 통해서 ResponseMessage를 생성한다. 
	 * 
	 * @param message 데이터 
	 * @return
	 * 		ResponseMessage 객체 
	 */
	protected ResponseMessage createSuccessMessage(Object message) {
		ResponseMessage resmessage = new ResponseMessage();
		HttpStatus hs = HttpStatus.OK;
		resmessage.setStatus(String.valueOf(hs.value()));  // 200 
		resmessage.setStatusText(hs.getReasonPhrase());    // OK 
		resmessage.setMessage(message);
		return resmessage;
	}//:

}///~
